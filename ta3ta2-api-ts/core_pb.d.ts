// package: 
// file: core.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_descriptor_pb from "google-protobuf/google/protobuf/descriptor_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as pipeline_pb from "./pipeline_pb";
import * as primitive_pb from "./primitive_pb";
import * as problem_pb from "./problem_pb";
import * as value_pb from "./value_pb";

export class ScoringConfiguration extends jspb.Message {
  getMethod(): EvaluationMethodMap[keyof EvaluationMethodMap];
  setMethod(value: EvaluationMethodMap[keyof EvaluationMethodMap]): void;

  getFolds(): number;
  setFolds(value: number): void;

  getTrainTestRatio(): number;
  setTrainTestRatio(value: number): void;

  getShuffle(): boolean;
  setShuffle(value: boolean): void;

  getRandomSeed(): number;
  setRandomSeed(value: number): void;

  getStratified(): boolean;
  setStratified(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ScoringConfiguration.AsObject;
  static toObject(includeInstance: boolean, msg: ScoringConfiguration): ScoringConfiguration.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ScoringConfiguration, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ScoringConfiguration;
  static deserializeBinaryFromReader(message: ScoringConfiguration, reader: jspb.BinaryReader): ScoringConfiguration;
}

export namespace ScoringConfiguration {
  export type AsObject = {
    method: EvaluationMethodMap[keyof EvaluationMethodMap],
    folds: number,
    trainTestRatio: number,
    shuffle: boolean,
    randomSeed: number,
    stratified: boolean,
  }
}

export class Score extends jspb.Message {
  hasMetric(): boolean;
  clearMetric(): void;
  getMetric(): problem_pb.ProblemPerformanceMetric | undefined;
  setMetric(value?: problem_pb.ProblemPerformanceMetric): void;

  getFold(): number;
  setFold(value: number): void;

  hasValue(): boolean;
  clearValue(): void;
  getValue(): value_pb.Value | undefined;
  setValue(value?: value_pb.Value): void;

  getRandomSeed(): number;
  setRandomSeed(value: number): void;

  hasNormalized(): boolean;
  clearNormalized(): void;
  getNormalized(): value_pb.Value | undefined;
  setNormalized(value?: value_pb.Value): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Score.AsObject;
  static toObject(includeInstance: boolean, msg: Score): Score.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Score, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Score;
  static deserializeBinaryFromReader(message: Score, reader: jspb.BinaryReader): Score;
}

export namespace Score {
  export type AsObject = {
    metric?: problem_pb.ProblemPerformanceMetric.AsObject,
    fold: number,
    value?: value_pb.Value.AsObject,
    randomSeed: number,
    normalized?: value_pb.Value.AsObject,
  }
}

export class Progress extends jspb.Message {
  getState(): ProgressStateMap[keyof ProgressStateMap];
  setState(value: ProgressStateMap[keyof ProgressStateMap]): void;

  getStatus(): string;
  setStatus(value: string): void;

  hasStart(): boolean;
  clearStart(): void;
  getStart(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setStart(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasEnd(): boolean;
  clearEnd(): void;
  getEnd(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setEnd(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Progress.AsObject;
  static toObject(includeInstance: boolean, msg: Progress): Progress.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Progress, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Progress;
  static deserializeBinaryFromReader(message: Progress, reader: jspb.BinaryReader): Progress;
}

export namespace Progress {
  export type AsObject = {
    state: ProgressStateMap[keyof ProgressStateMap],
    status: string,
    start?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    end?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class SearchSolutionsRequest extends jspb.Message {
  getUserAgent(): string;
  setUserAgent(value: string): void;

  getVersion(): string;
  setVersion(value: string): void;

  getTimeBoundSearch(): number;
  setTimeBoundSearch(value: number): void;

  getPriority(): number;
  setPriority(value: number): void;

  clearAllowedValueTypesList(): void;
  getAllowedValueTypesList(): Array<value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap]>;
  setAllowedValueTypesList(value: Array<value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap]>): void;
  addAllowedValueTypes(value: value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap], index?: number): value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap];

  hasProblem(): boolean;
  clearProblem(): void;
  getProblem(): problem_pb.ProblemDescription | undefined;
  setProblem(value?: problem_pb.ProblemDescription): void;

  hasTemplate(): boolean;
  clearTemplate(): void;
  getTemplate(): pipeline_pb.PipelineDescription | undefined;
  setTemplate(value?: pipeline_pb.PipelineDescription): void;

  clearInputsList(): void;
  getInputsList(): Array<value_pb.Value>;
  setInputsList(value: Array<value_pb.Value>): void;
  addInputs(value?: value_pb.Value, index?: number): value_pb.Value;

  getTimeBoundRun(): number;
  setTimeBoundRun(value: number): void;

  getRankSolutionsLimit(): number;
  setRankSolutionsLimit(value: number): void;

  getRandomSeed(): number;
  setRandomSeed(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SearchSolutionsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SearchSolutionsRequest): SearchSolutionsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SearchSolutionsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SearchSolutionsRequest;
  static deserializeBinaryFromReader(message: SearchSolutionsRequest, reader: jspb.BinaryReader): SearchSolutionsRequest;
}

export namespace SearchSolutionsRequest {
  export type AsObject = {
    userAgent: string,
    version: string,
    timeBoundSearch: number,
    priority: number,
    allowedValueTypesList: Array<value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap]>,
    problem?: problem_pb.ProblemDescription.AsObject,
    template?: pipeline_pb.PipelineDescription.AsObject,
    inputsList: Array<value_pb.Value.AsObject>,
    timeBoundRun: number,
    rankSolutionsLimit: number,
    randomSeed: number,
  }
}

export class SearchSolutionsResponse extends jspb.Message {
  getSearchId(): string;
  setSearchId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SearchSolutionsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SearchSolutionsResponse): SearchSolutionsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SearchSolutionsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SearchSolutionsResponse;
  static deserializeBinaryFromReader(message: SearchSolutionsResponse, reader: jspb.BinaryReader): SearchSolutionsResponse;
}

export namespace SearchSolutionsResponse {
  export type AsObject = {
    searchId: string,
  }
}

export class EndSearchSolutionsRequest extends jspb.Message {
  getSearchId(): string;
  setSearchId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EndSearchSolutionsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: EndSearchSolutionsRequest): EndSearchSolutionsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: EndSearchSolutionsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EndSearchSolutionsRequest;
  static deserializeBinaryFromReader(message: EndSearchSolutionsRequest, reader: jspb.BinaryReader): EndSearchSolutionsRequest;
}

export namespace EndSearchSolutionsRequest {
  export type AsObject = {
    searchId: string,
  }
}

export class EndSearchSolutionsResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EndSearchSolutionsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: EndSearchSolutionsResponse): EndSearchSolutionsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: EndSearchSolutionsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EndSearchSolutionsResponse;
  static deserializeBinaryFromReader(message: EndSearchSolutionsResponse, reader: jspb.BinaryReader): EndSearchSolutionsResponse;
}

export namespace EndSearchSolutionsResponse {
  export type AsObject = {
  }
}

export class StopSearchSolutionsRequest extends jspb.Message {
  getSearchId(): string;
  setSearchId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StopSearchSolutionsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: StopSearchSolutionsRequest): StopSearchSolutionsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StopSearchSolutionsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StopSearchSolutionsRequest;
  static deserializeBinaryFromReader(message: StopSearchSolutionsRequest, reader: jspb.BinaryReader): StopSearchSolutionsRequest;
}

export namespace StopSearchSolutionsRequest {
  export type AsObject = {
    searchId: string,
  }
}

export class StopSearchSolutionsResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StopSearchSolutionsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: StopSearchSolutionsResponse): StopSearchSolutionsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StopSearchSolutionsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StopSearchSolutionsResponse;
  static deserializeBinaryFromReader(message: StopSearchSolutionsResponse, reader: jspb.BinaryReader): StopSearchSolutionsResponse;
}

export namespace StopSearchSolutionsResponse {
  export type AsObject = {
  }
}

export class SolutionSearchScore extends jspb.Message {
  hasScoringConfiguration(): boolean;
  clearScoringConfiguration(): void;
  getScoringConfiguration(): ScoringConfiguration | undefined;
  setScoringConfiguration(value?: ScoringConfiguration): void;

  clearScoresList(): void;
  getScoresList(): Array<Score>;
  setScoresList(value: Array<Score>): void;
  addScores(value?: Score, index?: number): Score;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolutionSearchScore.AsObject;
  static toObject(includeInstance: boolean, msg: SolutionSearchScore): SolutionSearchScore.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SolutionSearchScore, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolutionSearchScore;
  static deserializeBinaryFromReader(message: SolutionSearchScore, reader: jspb.BinaryReader): SolutionSearchScore;
}

export namespace SolutionSearchScore {
  export type AsObject = {
    scoringConfiguration?: ScoringConfiguration.AsObject,
    scoresList: Array<Score.AsObject>,
  }
}

export class GetSearchSolutionsResultsRequest extends jspb.Message {
  getSearchId(): string;
  setSearchId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetSearchSolutionsResultsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetSearchSolutionsResultsRequest): GetSearchSolutionsResultsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetSearchSolutionsResultsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetSearchSolutionsResultsRequest;
  static deserializeBinaryFromReader(message: GetSearchSolutionsResultsRequest, reader: jspb.BinaryReader): GetSearchSolutionsResultsRequest;
}

export namespace GetSearchSolutionsResultsRequest {
  export type AsObject = {
    searchId: string,
  }
}

export class GetSearchSolutionsResultsResponse extends jspb.Message {
  hasProgress(): boolean;
  clearProgress(): void;
  getProgress(): Progress | undefined;
  setProgress(value?: Progress): void;

  getDoneTicks(): number;
  setDoneTicks(value: number): void;

  getAllTicks(): number;
  setAllTicks(value: number): void;

  getSolutionId(): string;
  setSolutionId(value: string): void;

  getInternalScore(): number;
  setInternalScore(value: number): void;

  clearScoresList(): void;
  getScoresList(): Array<SolutionSearchScore>;
  setScoresList(value: Array<SolutionSearchScore>): void;
  addScores(value?: SolutionSearchScore, index?: number): SolutionSearchScore;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetSearchSolutionsResultsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetSearchSolutionsResultsResponse): GetSearchSolutionsResultsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetSearchSolutionsResultsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetSearchSolutionsResultsResponse;
  static deserializeBinaryFromReader(message: GetSearchSolutionsResultsResponse, reader: jspb.BinaryReader): GetSearchSolutionsResultsResponse;
}

export namespace GetSearchSolutionsResultsResponse {
  export type AsObject = {
    progress?: Progress.AsObject,
    doneTicks: number,
    allTicks: number,
    solutionId: string,
    internalScore: number,
    scoresList: Array<SolutionSearchScore.AsObject>,
  }
}

export class DescribeSolutionRequest extends jspb.Message {
  getSolutionId(): string;
  setSolutionId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DescribeSolutionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DescribeSolutionRequest): DescribeSolutionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DescribeSolutionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DescribeSolutionRequest;
  static deserializeBinaryFromReader(message: DescribeSolutionRequest, reader: jspb.BinaryReader): DescribeSolutionRequest;
}

export namespace DescribeSolutionRequest {
  export type AsObject = {
    solutionId: string,
  }
}

export class PrimitiveStepDescription extends jspb.Message {
  getHyperparamsMap(): jspb.Map<string, value_pb.Value>;
  clearHyperparamsMap(): void;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PrimitiveStepDescription.AsObject;
  static toObject(includeInstance: boolean, msg: PrimitiveStepDescription): PrimitiveStepDescription.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PrimitiveStepDescription, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PrimitiveStepDescription;
  static deserializeBinaryFromReader(message: PrimitiveStepDescription, reader: jspb.BinaryReader): PrimitiveStepDescription;
}

export namespace PrimitiveStepDescription {
  export type AsObject = {
    hyperparamsMap: Array<[string, value_pb.Value.AsObject]>,
  }
}

export class SubpipelineStepDescription extends jspb.Message {
  clearStepsList(): void;
  getStepsList(): Array<StepDescription>;
  setStepsList(value: Array<StepDescription>): void;
  addSteps(value?: StepDescription, index?: number): StepDescription;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SubpipelineStepDescription.AsObject;
  static toObject(includeInstance: boolean, msg: SubpipelineStepDescription): SubpipelineStepDescription.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SubpipelineStepDescription, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SubpipelineStepDescription;
  static deserializeBinaryFromReader(message: SubpipelineStepDescription, reader: jspb.BinaryReader): SubpipelineStepDescription;
}

export namespace SubpipelineStepDescription {
  export type AsObject = {
    stepsList: Array<StepDescription.AsObject>,
  }
}

export class StepDescription extends jspb.Message {
  hasPrimitive(): boolean;
  clearPrimitive(): void;
  getPrimitive(): PrimitiveStepDescription | undefined;
  setPrimitive(value?: PrimitiveStepDescription): void;

  hasPipeline(): boolean;
  clearPipeline(): void;
  getPipeline(): SubpipelineStepDescription | undefined;
  setPipeline(value?: SubpipelineStepDescription): void;

  getStepCase(): StepDescription.StepCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StepDescription.AsObject;
  static toObject(includeInstance: boolean, msg: StepDescription): StepDescription.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StepDescription, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StepDescription;
  static deserializeBinaryFromReader(message: StepDescription, reader: jspb.BinaryReader): StepDescription;
}

export namespace StepDescription {
  export type AsObject = {
    primitive?: PrimitiveStepDescription.AsObject,
    pipeline?: SubpipelineStepDescription.AsObject,
  }

  export enum StepCase {
    STEP_NOT_SET = 0,
    PRIMITIVE = 1,
    PIPELINE = 2,
  }
}

export class DescribeSolutionResponse extends jspb.Message {
  hasPipeline(): boolean;
  clearPipeline(): void;
  getPipeline(): pipeline_pb.PipelineDescription | undefined;
  setPipeline(value?: pipeline_pb.PipelineDescription): void;

  clearStepsList(): void;
  getStepsList(): Array<StepDescription>;
  setStepsList(value: Array<StepDescription>): void;
  addSteps(value?: StepDescription, index?: number): StepDescription;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DescribeSolutionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DescribeSolutionResponse): DescribeSolutionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DescribeSolutionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DescribeSolutionResponse;
  static deserializeBinaryFromReader(message: DescribeSolutionResponse, reader: jspb.BinaryReader): DescribeSolutionResponse;
}

export namespace DescribeSolutionResponse {
  export type AsObject = {
    pipeline?: pipeline_pb.PipelineDescription.AsObject,
    stepsList: Array<StepDescription.AsObject>,
  }
}

export class StepProgress extends jspb.Message {
  hasProgress(): boolean;
  clearProgress(): void;
  getProgress(): Progress | undefined;
  setProgress(value?: Progress): void;

  clearStepsList(): void;
  getStepsList(): Array<StepProgress>;
  setStepsList(value: Array<StepProgress>): void;
  addSteps(value?: StepProgress, index?: number): StepProgress;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StepProgress.AsObject;
  static toObject(includeInstance: boolean, msg: StepProgress): StepProgress.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StepProgress, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StepProgress;
  static deserializeBinaryFromReader(message: StepProgress, reader: jspb.BinaryReader): StepProgress;
}

export namespace StepProgress {
  export type AsObject = {
    progress?: Progress.AsObject,
    stepsList: Array<StepProgress.AsObject>,
  }
}

export class SolutionRunUser extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getChosen(): boolean;
  setChosen(value: boolean): void;

  getReason(): string;
  setReason(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolutionRunUser.AsObject;
  static toObject(includeInstance: boolean, msg: SolutionRunUser): SolutionRunUser.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SolutionRunUser, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolutionRunUser;
  static deserializeBinaryFromReader(message: SolutionRunUser, reader: jspb.BinaryReader): SolutionRunUser;
}

export namespace SolutionRunUser {
  export type AsObject = {
    id: string,
    chosen: boolean,
    reason: string,
  }
}

export class ScoreSolutionRequest extends jspb.Message {
  getSolutionId(): string;
  setSolutionId(value: string): void;

  clearInputsList(): void;
  getInputsList(): Array<value_pb.Value>;
  setInputsList(value: Array<value_pb.Value>): void;
  addInputs(value?: value_pb.Value, index?: number): value_pb.Value;

  clearPerformanceMetricsList(): void;
  getPerformanceMetricsList(): Array<problem_pb.ProblemPerformanceMetric>;
  setPerformanceMetricsList(value: Array<problem_pb.ProblemPerformanceMetric>): void;
  addPerformanceMetrics(value?: problem_pb.ProblemPerformanceMetric, index?: number): problem_pb.ProblemPerformanceMetric;

  clearUsersList(): void;
  getUsersList(): Array<SolutionRunUser>;
  setUsersList(value: Array<SolutionRunUser>): void;
  addUsers(value?: SolutionRunUser, index?: number): SolutionRunUser;

  hasConfiguration(): boolean;
  clearConfiguration(): void;
  getConfiguration(): ScoringConfiguration | undefined;
  setConfiguration(value?: ScoringConfiguration): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ScoreSolutionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ScoreSolutionRequest): ScoreSolutionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ScoreSolutionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ScoreSolutionRequest;
  static deserializeBinaryFromReader(message: ScoreSolutionRequest, reader: jspb.BinaryReader): ScoreSolutionRequest;
}

export namespace ScoreSolutionRequest {
  export type AsObject = {
    solutionId: string,
    inputsList: Array<value_pb.Value.AsObject>,
    performanceMetricsList: Array<problem_pb.ProblemPerformanceMetric.AsObject>,
    usersList: Array<SolutionRunUser.AsObject>,
    configuration?: ScoringConfiguration.AsObject,
  }
}

export class ScoreSolutionResponse extends jspb.Message {
  getRequestId(): string;
  setRequestId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ScoreSolutionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ScoreSolutionResponse): ScoreSolutionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ScoreSolutionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ScoreSolutionResponse;
  static deserializeBinaryFromReader(message: ScoreSolutionResponse, reader: jspb.BinaryReader): ScoreSolutionResponse;
}

export namespace ScoreSolutionResponse {
  export type AsObject = {
    requestId: string,
  }
}

export class GetScoreSolutionResultsRequest extends jspb.Message {
  getRequestId(): string;
  setRequestId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetScoreSolutionResultsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetScoreSolutionResultsRequest): GetScoreSolutionResultsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetScoreSolutionResultsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetScoreSolutionResultsRequest;
  static deserializeBinaryFromReader(message: GetScoreSolutionResultsRequest, reader: jspb.BinaryReader): GetScoreSolutionResultsRequest;
}

export namespace GetScoreSolutionResultsRequest {
  export type AsObject = {
    requestId: string,
  }
}

export class GetScoreSolutionResultsResponse extends jspb.Message {
  hasProgress(): boolean;
  clearProgress(): void;
  getProgress(): Progress | undefined;
  setProgress(value?: Progress): void;

  clearScoresList(): void;
  getScoresList(): Array<Score>;
  setScoresList(value: Array<Score>): void;
  addScores(value?: Score, index?: number): Score;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetScoreSolutionResultsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetScoreSolutionResultsResponse): GetScoreSolutionResultsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetScoreSolutionResultsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetScoreSolutionResultsResponse;
  static deserializeBinaryFromReader(message: GetScoreSolutionResultsResponse, reader: jspb.BinaryReader): GetScoreSolutionResultsResponse;
}

export namespace GetScoreSolutionResultsResponse {
  export type AsObject = {
    progress?: Progress.AsObject,
    scoresList: Array<Score.AsObject>,
  }
}

export class FitSolutionRequest extends jspb.Message {
  getSolutionId(): string;
  setSolutionId(value: string): void;

  clearInputsList(): void;
  getInputsList(): Array<value_pb.Value>;
  setInputsList(value: Array<value_pb.Value>): void;
  addInputs(value?: value_pb.Value, index?: number): value_pb.Value;

  clearExposeOutputsList(): void;
  getExposeOutputsList(): Array<string>;
  setExposeOutputsList(value: Array<string>): void;
  addExposeOutputs(value: string, index?: number): string;

  clearExposeValueTypesList(): void;
  getExposeValueTypesList(): Array<value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap]>;
  setExposeValueTypesList(value: Array<value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap]>): void;
  addExposeValueTypes(value: value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap], index?: number): value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap];

  clearUsersList(): void;
  getUsersList(): Array<SolutionRunUser>;
  setUsersList(value: Array<SolutionRunUser>): void;
  addUsers(value?: SolutionRunUser, index?: number): SolutionRunUser;

  getRandomSeed(): number;
  setRandomSeed(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FitSolutionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: FitSolutionRequest): FitSolutionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FitSolutionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FitSolutionRequest;
  static deserializeBinaryFromReader(message: FitSolutionRequest, reader: jspb.BinaryReader): FitSolutionRequest;
}

export namespace FitSolutionRequest {
  export type AsObject = {
    solutionId: string,
    inputsList: Array<value_pb.Value.AsObject>,
    exposeOutputsList: Array<string>,
    exposeValueTypesList: Array<value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap]>,
    usersList: Array<SolutionRunUser.AsObject>,
    randomSeed: number,
  }
}

export class FitSolutionResponse extends jspb.Message {
  getRequestId(): string;
  setRequestId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FitSolutionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: FitSolutionResponse): FitSolutionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FitSolutionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FitSolutionResponse;
  static deserializeBinaryFromReader(message: FitSolutionResponse, reader: jspb.BinaryReader): FitSolutionResponse;
}

export namespace FitSolutionResponse {
  export type AsObject = {
    requestId: string,
  }
}

export class GetFitSolutionResultsRequest extends jspb.Message {
  getRequestId(): string;
  setRequestId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFitSolutionResultsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetFitSolutionResultsRequest): GetFitSolutionResultsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFitSolutionResultsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFitSolutionResultsRequest;
  static deserializeBinaryFromReader(message: GetFitSolutionResultsRequest, reader: jspb.BinaryReader): GetFitSolutionResultsRequest;
}

export namespace GetFitSolutionResultsRequest {
  export type AsObject = {
    requestId: string,
  }
}

export class GetFitSolutionResultsResponse extends jspb.Message {
  hasProgress(): boolean;
  clearProgress(): void;
  getProgress(): Progress | undefined;
  setProgress(value?: Progress): void;

  clearStepsList(): void;
  getStepsList(): Array<StepProgress>;
  setStepsList(value: Array<StepProgress>): void;
  addSteps(value?: StepProgress, index?: number): StepProgress;

  getExposedOutputsMap(): jspb.Map<string, value_pb.Value>;
  clearExposedOutputsMap(): void;
  getFittedSolutionId(): string;
  setFittedSolutionId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFitSolutionResultsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetFitSolutionResultsResponse): GetFitSolutionResultsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFitSolutionResultsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFitSolutionResultsResponse;
  static deserializeBinaryFromReader(message: GetFitSolutionResultsResponse, reader: jspb.BinaryReader): GetFitSolutionResultsResponse;
}

export namespace GetFitSolutionResultsResponse {
  export type AsObject = {
    progress?: Progress.AsObject,
    stepsList: Array<StepProgress.AsObject>,
    exposedOutputsMap: Array<[string, value_pb.Value.AsObject]>,
    fittedSolutionId: string,
  }
}

export class ProduceSolutionRequest extends jspb.Message {
  getFittedSolutionId(): string;
  setFittedSolutionId(value: string): void;

  clearInputsList(): void;
  getInputsList(): Array<value_pb.Value>;
  setInputsList(value: Array<value_pb.Value>): void;
  addInputs(value?: value_pb.Value, index?: number): value_pb.Value;

  clearExposeOutputsList(): void;
  getExposeOutputsList(): Array<string>;
  setExposeOutputsList(value: Array<string>): void;
  addExposeOutputs(value: string, index?: number): string;

  clearExposeValueTypesList(): void;
  getExposeValueTypesList(): Array<value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap]>;
  setExposeValueTypesList(value: Array<value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap]>): void;
  addExposeValueTypes(value: value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap], index?: number): value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap];

  clearUsersList(): void;
  getUsersList(): Array<SolutionRunUser>;
  setUsersList(value: Array<SolutionRunUser>): void;
  addUsers(value?: SolutionRunUser, index?: number): SolutionRunUser;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProduceSolutionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ProduceSolutionRequest): ProduceSolutionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProduceSolutionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProduceSolutionRequest;
  static deserializeBinaryFromReader(message: ProduceSolutionRequest, reader: jspb.BinaryReader): ProduceSolutionRequest;
}

export namespace ProduceSolutionRequest {
  export type AsObject = {
    fittedSolutionId: string,
    inputsList: Array<value_pb.Value.AsObject>,
    exposeOutputsList: Array<string>,
    exposeValueTypesList: Array<value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap]>,
    usersList: Array<SolutionRunUser.AsObject>,
  }
}

export class ProduceSolutionResponse extends jspb.Message {
  getRequestId(): string;
  setRequestId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProduceSolutionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ProduceSolutionResponse): ProduceSolutionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProduceSolutionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProduceSolutionResponse;
  static deserializeBinaryFromReader(message: ProduceSolutionResponse, reader: jspb.BinaryReader): ProduceSolutionResponse;
}

export namespace ProduceSolutionResponse {
  export type AsObject = {
    requestId: string,
  }
}

export class GetProduceSolutionResultsRequest extends jspb.Message {
  getRequestId(): string;
  setRequestId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProduceSolutionResultsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetProduceSolutionResultsRequest): GetProduceSolutionResultsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProduceSolutionResultsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProduceSolutionResultsRequest;
  static deserializeBinaryFromReader(message: GetProduceSolutionResultsRequest, reader: jspb.BinaryReader): GetProduceSolutionResultsRequest;
}

export namespace GetProduceSolutionResultsRequest {
  export type AsObject = {
    requestId: string,
  }
}

export class GetProduceSolutionResultsResponse extends jspb.Message {
  hasProgress(): boolean;
  clearProgress(): void;
  getProgress(): Progress | undefined;
  setProgress(value?: Progress): void;

  clearStepsList(): void;
  getStepsList(): Array<StepProgress>;
  setStepsList(value: Array<StepProgress>): void;
  addSteps(value?: StepProgress, index?: number): StepProgress;

  getExposedOutputsMap(): jspb.Map<string, value_pb.Value>;
  clearExposedOutputsMap(): void;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProduceSolutionResultsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetProduceSolutionResultsResponse): GetProduceSolutionResultsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProduceSolutionResultsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProduceSolutionResultsResponse;
  static deserializeBinaryFromReader(message: GetProduceSolutionResultsResponse, reader: jspb.BinaryReader): GetProduceSolutionResultsResponse;
}

export namespace GetProduceSolutionResultsResponse {
  export type AsObject = {
    progress?: Progress.AsObject,
    stepsList: Array<StepProgress.AsObject>,
    exposedOutputsMap: Array<[string, value_pb.Value.AsObject]>,
  }
}

export class SolutionExportRequest extends jspb.Message {
  getSolutionId(): string;
  setSolutionId(value: string): void;

  getRank(): number;
  setRank(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolutionExportRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SolutionExportRequest): SolutionExportRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SolutionExportRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolutionExportRequest;
  static deserializeBinaryFromReader(message: SolutionExportRequest, reader: jspb.BinaryReader): SolutionExportRequest;
}

export namespace SolutionExportRequest {
  export type AsObject = {
    solutionId: string,
    rank: number,
  }
}

export class SolutionExportResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolutionExportResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SolutionExportResponse): SolutionExportResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SolutionExportResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolutionExportResponse;
  static deserializeBinaryFromReader(message: SolutionExportResponse, reader: jspb.BinaryReader): SolutionExportResponse;
}

export namespace SolutionExportResponse {
  export type AsObject = {
  }
}

export class DataAvailableRequest extends jspb.Message {
  getUserAgent(): string;
  setUserAgent(value: string): void;

  getVersion(): string;
  setVersion(value: string): void;

  getTimeBound(): number;
  setTimeBound(value: number): void;

  getPriority(): number;
  setPriority(value: number): void;

  clearDataList(): void;
  getDataList(): Array<value_pb.Value>;
  setDataList(value: Array<value_pb.Value>): void;
  addData(value?: value_pb.Value, index?: number): value_pb.Value;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DataAvailableRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DataAvailableRequest): DataAvailableRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DataAvailableRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DataAvailableRequest;
  static deserializeBinaryFromReader(message: DataAvailableRequest, reader: jspb.BinaryReader): DataAvailableRequest;
}

export namespace DataAvailableRequest {
  export type AsObject = {
    userAgent: string,
    version: string,
    timeBound: number,
    priority: number,
    dataList: Array<value_pb.Value.AsObject>,
  }
}

export class DataAvailableResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DataAvailableResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DataAvailableResponse): DataAvailableResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DataAvailableResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DataAvailableResponse;
  static deserializeBinaryFromReader(message: DataAvailableResponse, reader: jspb.BinaryReader): DataAvailableResponse;
}

export namespace DataAvailableResponse {
  export type AsObject = {
  }
}

export class SplitDataRequest extends jspb.Message {
  hasInput(): boolean;
  clearInput(): void;
  getInput(): value_pb.Value | undefined;
  setInput(value?: value_pb.Value): void;

  hasScoringConfiguration(): boolean;
  clearScoringConfiguration(): void;
  getScoringConfiguration(): ScoringConfiguration | undefined;
  setScoringConfiguration(value?: ScoringConfiguration): void;

  clearAllowedValueTypesList(): void;
  getAllowedValueTypesList(): Array<value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap]>;
  setAllowedValueTypesList(value: Array<value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap]>): void;
  addAllowedValueTypes(value: value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap], index?: number): value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap];

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SplitDataRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SplitDataRequest): SplitDataRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SplitDataRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SplitDataRequest;
  static deserializeBinaryFromReader(message: SplitDataRequest, reader: jspb.BinaryReader): SplitDataRequest;
}

export namespace SplitDataRequest {
  export type AsObject = {
    input?: value_pb.Value.AsObject,
    scoringConfiguration?: ScoringConfiguration.AsObject,
    allowedValueTypesList: Array<value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap]>,
  }
}

export class SplitDataResponse extends jspb.Message {
  hasTrainOutput(): boolean;
  clearTrainOutput(): void;
  getTrainOutput(): value_pb.Value | undefined;
  setTrainOutput(value?: value_pb.Value): void;

  hasTestOutput(): boolean;
  clearTestOutput(): void;
  getTestOutput(): value_pb.Value | undefined;
  setTestOutput(value?: value_pb.Value): void;

  hasScoreOutput(): boolean;
  clearScoreOutput(): void;
  getScoreOutput(): value_pb.Value | undefined;
  setScoreOutput(value?: value_pb.Value): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SplitDataResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SplitDataResponse): SplitDataResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SplitDataResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SplitDataResponse;
  static deserializeBinaryFromReader(message: SplitDataResponse, reader: jspb.BinaryReader): SplitDataResponse;
}

export namespace SplitDataResponse {
  export type AsObject = {
    trainOutput?: value_pb.Value.AsObject,
    testOutput?: value_pb.Value.AsObject,
    scoreOutput?: value_pb.Value.AsObject,
  }
}

export class ScorePredictionsRequest extends jspb.Message {
  hasPredictions(): boolean;
  clearPredictions(): void;
  getPredictions(): value_pb.Value | undefined;
  setPredictions(value?: value_pb.Value): void;

  hasScoreInput(): boolean;
  clearScoreInput(): void;
  getScoreInput(): value_pb.Value | undefined;
  setScoreInput(value?: value_pb.Value): void;

  hasProblem(): boolean;
  clearProblem(): void;
  getProblem(): problem_pb.ProblemDescription | undefined;
  setProblem(value?: problem_pb.ProblemDescription): void;

  clearMetricList(): void;
  getMetricList(): Array<problem_pb.ProblemPerformanceMetric>;
  setMetricList(value: Array<problem_pb.ProblemPerformanceMetric>): void;
  addMetric(value?: problem_pb.ProblemPerformanceMetric, index?: number): problem_pb.ProblemPerformanceMetric;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ScorePredictionsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ScorePredictionsRequest): ScorePredictionsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ScorePredictionsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ScorePredictionsRequest;
  static deserializeBinaryFromReader(message: ScorePredictionsRequest, reader: jspb.BinaryReader): ScorePredictionsRequest;
}

export namespace ScorePredictionsRequest {
  export type AsObject = {
    predictions?: value_pb.Value.AsObject,
    scoreInput?: value_pb.Value.AsObject,
    problem?: problem_pb.ProblemDescription.AsObject,
    metricList: Array<problem_pb.ProblemPerformanceMetric.AsObject>,
  }
}

export class ScorePredictionsResponse extends jspb.Message {
  clearScoresList(): void;
  getScoresList(): Array<Score>;
  setScoresList(value: Array<Score>): void;
  addScores(value?: Score, index?: number): Score;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ScorePredictionsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ScorePredictionsResponse): ScorePredictionsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ScorePredictionsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ScorePredictionsResponse;
  static deserializeBinaryFromReader(message: ScorePredictionsResponse, reader: jspb.BinaryReader): ScorePredictionsResponse;
}

export namespace ScorePredictionsResponse {
  export type AsObject = {
    scoresList: Array<Score.AsObject>,
  }
}

export class SaveSolutionRequest extends jspb.Message {
  getSolutionId(): string;
  setSolutionId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveSolutionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveSolutionRequest): SaveSolutionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveSolutionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveSolutionRequest;
  static deserializeBinaryFromReader(message: SaveSolutionRequest, reader: jspb.BinaryReader): SaveSolutionRequest;
}

export namespace SaveSolutionRequest {
  export type AsObject = {
    solutionId: string,
  }
}

export class SaveSolutionResponse extends jspb.Message {
  getSolutionUri(): string;
  setSolutionUri(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveSolutionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveSolutionResponse): SaveSolutionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveSolutionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveSolutionResponse;
  static deserializeBinaryFromReader(message: SaveSolutionResponse, reader: jspb.BinaryReader): SaveSolutionResponse;
}

export namespace SaveSolutionResponse {
  export type AsObject = {
    solutionUri: string,
  }
}

export class LoadSolutionRequest extends jspb.Message {
  getSolutionUri(): string;
  setSolutionUri(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoadSolutionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: LoadSolutionRequest): LoadSolutionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LoadSolutionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoadSolutionRequest;
  static deserializeBinaryFromReader(message: LoadSolutionRequest, reader: jspb.BinaryReader): LoadSolutionRequest;
}

export namespace LoadSolutionRequest {
  export type AsObject = {
    solutionUri: string,
  }
}

export class LoadSolutionResponse extends jspb.Message {
  getSolutionId(): string;
  setSolutionId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoadSolutionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: LoadSolutionResponse): LoadSolutionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LoadSolutionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoadSolutionResponse;
  static deserializeBinaryFromReader(message: LoadSolutionResponse, reader: jspb.BinaryReader): LoadSolutionResponse;
}

export namespace LoadSolutionResponse {
  export type AsObject = {
    solutionId: string,
  }
}

export class SaveFittedSolutionRequest extends jspb.Message {
  getFittedSolutionId(): string;
  setFittedSolutionId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveFittedSolutionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveFittedSolutionRequest): SaveFittedSolutionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveFittedSolutionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveFittedSolutionRequest;
  static deserializeBinaryFromReader(message: SaveFittedSolutionRequest, reader: jspb.BinaryReader): SaveFittedSolutionRequest;
}

export namespace SaveFittedSolutionRequest {
  export type AsObject = {
    fittedSolutionId: string,
  }
}

export class SaveFittedSolutionResponse extends jspb.Message {
  getFittedSolutionUri(): string;
  setFittedSolutionUri(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveFittedSolutionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveFittedSolutionResponse): SaveFittedSolutionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveFittedSolutionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveFittedSolutionResponse;
  static deserializeBinaryFromReader(message: SaveFittedSolutionResponse, reader: jspb.BinaryReader): SaveFittedSolutionResponse;
}

export namespace SaveFittedSolutionResponse {
  export type AsObject = {
    fittedSolutionUri: string,
  }
}

export class LoadFittedSolutionRequest extends jspb.Message {
  getFittedSolutionUri(): string;
  setFittedSolutionUri(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoadFittedSolutionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: LoadFittedSolutionRequest): LoadFittedSolutionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LoadFittedSolutionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoadFittedSolutionRequest;
  static deserializeBinaryFromReader(message: LoadFittedSolutionRequest, reader: jspb.BinaryReader): LoadFittedSolutionRequest;
}

export namespace LoadFittedSolutionRequest {
  export type AsObject = {
    fittedSolutionUri: string,
  }
}

export class LoadFittedSolutionResponse extends jspb.Message {
  getFittedSolutionId(): string;
  setFittedSolutionId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoadFittedSolutionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: LoadFittedSolutionResponse): LoadFittedSolutionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LoadFittedSolutionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoadFittedSolutionResponse;
  static deserializeBinaryFromReader(message: LoadFittedSolutionResponse, reader: jspb.BinaryReader): LoadFittedSolutionResponse;
}

export namespace LoadFittedSolutionResponse {
  export type AsObject = {
    fittedSolutionId: string,
  }
}

export class ListPrimitivesRequest extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPrimitivesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPrimitivesRequest): ListPrimitivesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPrimitivesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPrimitivesRequest;
  static deserializeBinaryFromReader(message: ListPrimitivesRequest, reader: jspb.BinaryReader): ListPrimitivesRequest;
}

export namespace ListPrimitivesRequest {
  export type AsObject = {
  }
}

export class ListPrimitivesResponse extends jspb.Message {
  clearPrimitivesList(): void;
  getPrimitivesList(): Array<primitive_pb.Primitive>;
  setPrimitivesList(value: Array<primitive_pb.Primitive>): void;
  addPrimitives(value?: primitive_pb.Primitive, index?: number): primitive_pb.Primitive;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPrimitivesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListPrimitivesResponse): ListPrimitivesResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPrimitivesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPrimitivesResponse;
  static deserializeBinaryFromReader(message: ListPrimitivesResponse, reader: jspb.BinaryReader): ListPrimitivesResponse;
}

export namespace ListPrimitivesResponse {
  export type AsObject = {
    primitivesList: Array<primitive_pb.Primitive.AsObject>,
  }
}

export class HelloRequest extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): HelloRequest.AsObject;
  static toObject(includeInstance: boolean, msg: HelloRequest): HelloRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: HelloRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): HelloRequest;
  static deserializeBinaryFromReader(message: HelloRequest, reader: jspb.BinaryReader): HelloRequest;
}

export namespace HelloRequest {
  export type AsObject = {
  }
}

export class HelloResponse extends jspb.Message {
  getUserAgent(): string;
  setUserAgent(value: string): void;

  getVersion(): string;
  setVersion(value: string): void;

  clearAllowedValueTypesList(): void;
  getAllowedValueTypesList(): Array<value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap]>;
  setAllowedValueTypesList(value: Array<value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap]>): void;
  addAllowedValueTypes(value: value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap], index?: number): value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap];

  clearSupportedExtensionsList(): void;
  getSupportedExtensionsList(): Array<string>;
  setSupportedExtensionsList(value: Array<string>): void;
  addSupportedExtensions(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): HelloResponse.AsObject;
  static toObject(includeInstance: boolean, msg: HelloResponse): HelloResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: HelloResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): HelloResponse;
  static deserializeBinaryFromReader(message: HelloResponse, reader: jspb.BinaryReader): HelloResponse;
}

export namespace HelloResponse {
  export type AsObject = {
    userAgent: string,
    version: string,
    allowedValueTypesList: Array<value_pb.ValueTypeMap[keyof value_pb.ValueTypeMap]>,
    supportedExtensionsList: Array<string>,
  }
}

  export const protocolVersion: jspb.ExtensionFieldInfo<string>;

export interface EvaluationMethodMap {
  EVALUATION_METHOD_UNDEFINED: 0;
  HOLDOUT: 1;
  K_FOLD: 2;
  RANKING: 99;
  LEAVE_ONE_OUT: 100;
  PREDICTION: 101;
  TRAINING_DATA: 102;
}

export const EvaluationMethod: EvaluationMethodMap;

export interface ProgressStateMap {
  PROGRESS_UNKNOWN: 0;
  PENDING: 1;
  RUNNING: 2;
  COMPLETED: 3;
  ERRORED: 4;
}

export const ProgressState: ProgressStateMap;

