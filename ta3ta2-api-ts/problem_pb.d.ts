// package: 
// file: problem.proto

import * as jspb from "google-protobuf";

export class ProblemPerformanceMetric extends jspb.Message {
  getMetric(): PerformanceMetricMap[keyof PerformanceMetricMap];
  setMetric(value: PerformanceMetricMap[keyof PerformanceMetricMap]): void;

  getK(): number;
  setK(value: number): void;

  getPosLabel(): string;
  setPosLabel(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProblemPerformanceMetric.AsObject;
  static toObject(includeInstance: boolean, msg: ProblemPerformanceMetric): ProblemPerformanceMetric.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProblemPerformanceMetric, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProblemPerformanceMetric;
  static deserializeBinaryFromReader(message: ProblemPerformanceMetric, reader: jspb.BinaryReader): ProblemPerformanceMetric;
}

export namespace ProblemPerformanceMetric {
  export type AsObject = {
    metric: PerformanceMetricMap[keyof PerformanceMetricMap],
    k: number,
    posLabel: string,
  }
}

export class Problem extends jspb.Message {
  clearTaskKeywordsList(): void;
  getTaskKeywordsList(): Array<TaskKeywordMap[keyof TaskKeywordMap]>;
  setTaskKeywordsList(value: Array<TaskKeywordMap[keyof TaskKeywordMap]>): void;
  addTaskKeywords(value: TaskKeywordMap[keyof TaskKeywordMap], index?: number): TaskKeywordMap[keyof TaskKeywordMap];

  clearPerformanceMetricsList(): void;
  getPerformanceMetricsList(): Array<ProblemPerformanceMetric>;
  setPerformanceMetricsList(value: Array<ProblemPerformanceMetric>): void;
  addPerformanceMetrics(value?: ProblemPerformanceMetric, index?: number): ProblemPerformanceMetric;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Problem.AsObject;
  static toObject(includeInstance: boolean, msg: Problem): Problem.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Problem, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Problem;
  static deserializeBinaryFromReader(message: Problem, reader: jspb.BinaryReader): Problem;
}

export namespace Problem {
  export type AsObject = {
    taskKeywordsList: Array<TaskKeywordMap[keyof TaskKeywordMap]>,
    performanceMetricsList: Array<ProblemPerformanceMetric.AsObject>,
  }
}

export class ProblemTarget extends jspb.Message {
  getTargetIndex(): number;
  setTargetIndex(value: number): void;

  getResourceId(): string;
  setResourceId(value: string): void;

  getColumnIndex(): number;
  setColumnIndex(value: number): void;

  getColumnName(): string;
  setColumnName(value: string): void;

  getClustersNumber(): number;
  setClustersNumber(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProblemTarget.AsObject;
  static toObject(includeInstance: boolean, msg: ProblemTarget): ProblemTarget.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProblemTarget, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProblemTarget;
  static deserializeBinaryFromReader(message: ProblemTarget, reader: jspb.BinaryReader): ProblemTarget;
}

export namespace ProblemTarget {
  export type AsObject = {
    targetIndex: number,
    resourceId: string,
    columnIndex: number,
    columnName: string,
    clustersNumber: number,
  }
}

export class ProblemPrivilegedData extends jspb.Message {
  getPrivilegedDataIndex(): number;
  setPrivilegedDataIndex(value: number): void;

  getResourceId(): string;
  setResourceId(value: string): void;

  getColumnIndex(): number;
  setColumnIndex(value: number): void;

  getColumnName(): string;
  setColumnName(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProblemPrivilegedData.AsObject;
  static toObject(includeInstance: boolean, msg: ProblemPrivilegedData): ProblemPrivilegedData.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProblemPrivilegedData, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProblemPrivilegedData;
  static deserializeBinaryFromReader(message: ProblemPrivilegedData, reader: jspb.BinaryReader): ProblemPrivilegedData;
}

export namespace ProblemPrivilegedData {
  export type AsObject = {
    privilegedDataIndex: number,
    resourceId: string,
    columnIndex: number,
    columnName: string,
  }
}

export class ForecastingHorizon extends jspb.Message {
  getResourceId(): string;
  setResourceId(value: string): void;

  getColumnIndex(): number;
  setColumnIndex(value: number): void;

  getColumnName(): string;
  setColumnName(value: string): void;

  getHorizonValue(): number;
  setHorizonValue(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ForecastingHorizon.AsObject;
  static toObject(includeInstance: boolean, msg: ForecastingHorizon): ForecastingHorizon.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ForecastingHorizon, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ForecastingHorizon;
  static deserializeBinaryFromReader(message: ForecastingHorizon, reader: jspb.BinaryReader): ForecastingHorizon;
}

export namespace ForecastingHorizon {
  export type AsObject = {
    resourceId: string,
    columnIndex: number,
    columnName: string,
    horizonValue: number,
  }
}

export class ProblemInput extends jspb.Message {
  getDatasetId(): string;
  setDatasetId(value: string): void;

  clearTargetsList(): void;
  getTargetsList(): Array<ProblemTarget>;
  setTargetsList(value: Array<ProblemTarget>): void;
  addTargets(value?: ProblemTarget, index?: number): ProblemTarget;

  clearPrivilegedDataList(): void;
  getPrivilegedDataList(): Array<ProblemPrivilegedData>;
  setPrivilegedDataList(value: Array<ProblemPrivilegedData>): void;
  addPrivilegedData(value?: ProblemPrivilegedData, index?: number): ProblemPrivilegedData;

  hasForecastingHorizon(): boolean;
  clearForecastingHorizon(): void;
  getForecastingHorizon(): ForecastingHorizon | undefined;
  setForecastingHorizon(value?: ForecastingHorizon): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProblemInput.AsObject;
  static toObject(includeInstance: boolean, msg: ProblemInput): ProblemInput.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProblemInput, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProblemInput;
  static deserializeBinaryFromReader(message: ProblemInput, reader: jspb.BinaryReader): ProblemInput;
}

export namespace ProblemInput {
  export type AsObject = {
    datasetId: string,
    targetsList: Array<ProblemTarget.AsObject>,
    privilegedDataList: Array<ProblemPrivilegedData.AsObject>,
    forecastingHorizon?: ForecastingHorizon.AsObject,
  }
}

export class DataAugmentation extends jspb.Message {
  clearDomainList(): void;
  getDomainList(): Array<string>;
  setDomainList(value: Array<string>): void;
  addDomain(value: string, index?: number): string;

  clearKeywordsList(): void;
  getKeywordsList(): Array<string>;
  setKeywordsList(value: Array<string>): void;
  addKeywords(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DataAugmentation.AsObject;
  static toObject(includeInstance: boolean, msg: DataAugmentation): DataAugmentation.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DataAugmentation, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DataAugmentation;
  static deserializeBinaryFromReader(message: DataAugmentation, reader: jspb.BinaryReader): DataAugmentation;
}

export namespace DataAugmentation {
  export type AsObject = {
    domainList: Array<string>,
    keywordsList: Array<string>,
  }
}

export class ProblemDescription extends jspb.Message {
  hasProblem(): boolean;
  clearProblem(): void;
  getProblem(): Problem | undefined;
  setProblem(value?: Problem): void;

  clearInputsList(): void;
  getInputsList(): Array<ProblemInput>;
  setInputsList(value: Array<ProblemInput>): void;
  addInputs(value?: ProblemInput, index?: number): ProblemInput;

  getId(): string;
  setId(value: string): void;

  getVersion(): string;
  setVersion(value: string): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  getDigest(): string;
  setDigest(value: string): void;

  clearDataAugmentationList(): void;
  getDataAugmentationList(): Array<DataAugmentation>;
  setDataAugmentationList(value: Array<DataAugmentation>): void;
  addDataAugmentation(value?: DataAugmentation, index?: number): DataAugmentation;

  clearOtherNamesList(): void;
  getOtherNamesList(): Array<string>;
  setOtherNamesList(value: Array<string>): void;
  addOtherNames(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProblemDescription.AsObject;
  static toObject(includeInstance: boolean, msg: ProblemDescription): ProblemDescription.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProblemDescription, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProblemDescription;
  static deserializeBinaryFromReader(message: ProblemDescription, reader: jspb.BinaryReader): ProblemDescription;
}

export namespace ProblemDescription {
  export type AsObject = {
    problem?: Problem.AsObject,
    inputsList: Array<ProblemInput.AsObject>,
    id: string,
    version: string,
    name: string,
    description: string,
    digest: string,
    dataAugmentationList: Array<DataAugmentation.AsObject>,
    otherNamesList: Array<string>,
  }
}

export interface TaskKeywordMap {
  TASK_KEYWORD_UNDEFINED: 0;
  CLASSIFICATION: 1;
  REGRESSION: 2;
  CLUSTERING: 3;
  LINK_PREDICTION: 4;
  VERTEX_NOMINATION: 5;
  VERTEX_CLASSIFICATION: 6;
  COMMUNITY_DETECTION: 7;
  GRAPH_MATCHING: 8;
  FORECASTING: 9;
  COLLABORATIVE_FILTERING: 10;
  OBJECT_DETECTION: 11;
  SEMISUPERVISED: 12;
  BINARY: 13;
  MULTICLASS: 14;
  MULTILABEL: 15;
  UNIVARIATE: 16;
  MULTIVARIATE: 17;
  OVERLAPPING: 18;
  NONOVERLAPPING: 19;
  TABULAR: 20;
  RELATIONAL: 21;
  IMAGE: 22;
  AUDIO: 23;
  VIDEO: 24;
  SPEECH: 25;
  TEXT: 26;
  GRAPH: 27;
  MULTIGRAPH: 28;
  TIME_SERIES: 29;
  GROUPED: 30;
  GEOSPATIAL: 31;
  REMOTE_SENSING: 32;
  LUPI: 33;
  MISSING_METADATA: 34;
}

export const TaskKeyword: TaskKeywordMap;

export interface PerformanceMetricMap {
  METRIC_UNDEFINED: 0;
  ACCURACY: 1;
  PRECISION: 2;
  RECALL: 3;
  F1: 4;
  F1_MICRO: 5;
  F1_MACRO: 6;
  ROC_AUC: 7;
  ROC_AUC_MICRO: 8;
  ROC_AUC_MACRO: 9;
  MEAN_SQUARED_ERROR: 10;
  ROOT_MEAN_SQUARED_ERROR: 11;
  MEAN_ABSOLUTE_ERROR: 12;
  R_SQUARED: 13;
  NORMALIZED_MUTUAL_INFORMATION: 14;
  JACCARD_SIMILARITY_SCORE: 15;
  PRECISION_AT_TOP_K: 16;
  OBJECT_DETECTION_AVERAGE_PRECISION: 17;
  HAMMING_LOSS: 18;
  RANK: 99;
  LOSS: 100;
}

export const PerformanceMetric: PerformanceMetricMap;

