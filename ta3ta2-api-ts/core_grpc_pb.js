// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var core_pb = require('./core_pb.js');
var google_protobuf_descriptor_pb = require('google-protobuf/google/protobuf/descriptor_pb.js');
var google_protobuf_timestamp_pb = require('google-protobuf/google/protobuf/timestamp_pb.js');
var pipeline_pb = require('./pipeline_pb.js');
var primitive_pb = require('./primitive_pb.js');
var problem_pb = require('./problem_pb.js');
var value_pb = require('./value_pb.js');

function serialize_DataAvailableRequest(arg) {
  if (!(arg instanceof core_pb.DataAvailableRequest)) {
    throw new Error('Expected argument of type DataAvailableRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_DataAvailableRequest(buffer_arg) {
  return core_pb.DataAvailableRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_DataAvailableResponse(arg) {
  if (!(arg instanceof core_pb.DataAvailableResponse)) {
    throw new Error('Expected argument of type DataAvailableResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_DataAvailableResponse(buffer_arg) {
  return core_pb.DataAvailableResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_DescribeSolutionRequest(arg) {
  if (!(arg instanceof core_pb.DescribeSolutionRequest)) {
    throw new Error('Expected argument of type DescribeSolutionRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_DescribeSolutionRequest(buffer_arg) {
  return core_pb.DescribeSolutionRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_DescribeSolutionResponse(arg) {
  if (!(arg instanceof core_pb.DescribeSolutionResponse)) {
    throw new Error('Expected argument of type DescribeSolutionResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_DescribeSolutionResponse(buffer_arg) {
  return core_pb.DescribeSolutionResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_EndSearchSolutionsRequest(arg) {
  if (!(arg instanceof core_pb.EndSearchSolutionsRequest)) {
    throw new Error('Expected argument of type EndSearchSolutionsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_EndSearchSolutionsRequest(buffer_arg) {
  return core_pb.EndSearchSolutionsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_EndSearchSolutionsResponse(arg) {
  if (!(arg instanceof core_pb.EndSearchSolutionsResponse)) {
    throw new Error('Expected argument of type EndSearchSolutionsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_EndSearchSolutionsResponse(buffer_arg) {
  return core_pb.EndSearchSolutionsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_FitSolutionRequest(arg) {
  if (!(arg instanceof core_pb.FitSolutionRequest)) {
    throw new Error('Expected argument of type FitSolutionRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_FitSolutionRequest(buffer_arg) {
  return core_pb.FitSolutionRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_FitSolutionResponse(arg) {
  if (!(arg instanceof core_pb.FitSolutionResponse)) {
    throw new Error('Expected argument of type FitSolutionResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_FitSolutionResponse(buffer_arg) {
  return core_pb.FitSolutionResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_GetFitSolutionResultsRequest(arg) {
  if (!(arg instanceof core_pb.GetFitSolutionResultsRequest)) {
    throw new Error('Expected argument of type GetFitSolutionResultsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_GetFitSolutionResultsRequest(buffer_arg) {
  return core_pb.GetFitSolutionResultsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_GetFitSolutionResultsResponse(arg) {
  if (!(arg instanceof core_pb.GetFitSolutionResultsResponse)) {
    throw new Error('Expected argument of type GetFitSolutionResultsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_GetFitSolutionResultsResponse(buffer_arg) {
  return core_pb.GetFitSolutionResultsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_GetProduceSolutionResultsRequest(arg) {
  if (!(arg instanceof core_pb.GetProduceSolutionResultsRequest)) {
    throw new Error('Expected argument of type GetProduceSolutionResultsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_GetProduceSolutionResultsRequest(buffer_arg) {
  return core_pb.GetProduceSolutionResultsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_GetProduceSolutionResultsResponse(arg) {
  if (!(arg instanceof core_pb.GetProduceSolutionResultsResponse)) {
    throw new Error('Expected argument of type GetProduceSolutionResultsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_GetProduceSolutionResultsResponse(buffer_arg) {
  return core_pb.GetProduceSolutionResultsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_GetScoreSolutionResultsRequest(arg) {
  if (!(arg instanceof core_pb.GetScoreSolutionResultsRequest)) {
    throw new Error('Expected argument of type GetScoreSolutionResultsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_GetScoreSolutionResultsRequest(buffer_arg) {
  return core_pb.GetScoreSolutionResultsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_GetScoreSolutionResultsResponse(arg) {
  if (!(arg instanceof core_pb.GetScoreSolutionResultsResponse)) {
    throw new Error('Expected argument of type GetScoreSolutionResultsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_GetScoreSolutionResultsResponse(buffer_arg) {
  return core_pb.GetScoreSolutionResultsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_GetSearchSolutionsResultsRequest(arg) {
  if (!(arg instanceof core_pb.GetSearchSolutionsResultsRequest)) {
    throw new Error('Expected argument of type GetSearchSolutionsResultsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_GetSearchSolutionsResultsRequest(buffer_arg) {
  return core_pb.GetSearchSolutionsResultsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_GetSearchSolutionsResultsResponse(arg) {
  if (!(arg instanceof core_pb.GetSearchSolutionsResultsResponse)) {
    throw new Error('Expected argument of type GetSearchSolutionsResultsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_GetSearchSolutionsResultsResponse(buffer_arg) {
  return core_pb.GetSearchSolutionsResultsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_HelloRequest(arg) {
  if (!(arg instanceof core_pb.HelloRequest)) {
    throw new Error('Expected argument of type HelloRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_HelloRequest(buffer_arg) {
  return core_pb.HelloRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_HelloResponse(arg) {
  if (!(arg instanceof core_pb.HelloResponse)) {
    throw new Error('Expected argument of type HelloResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_HelloResponse(buffer_arg) {
  return core_pb.HelloResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ListPrimitivesRequest(arg) {
  if (!(arg instanceof core_pb.ListPrimitivesRequest)) {
    throw new Error('Expected argument of type ListPrimitivesRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_ListPrimitivesRequest(buffer_arg) {
  return core_pb.ListPrimitivesRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ListPrimitivesResponse(arg) {
  if (!(arg instanceof core_pb.ListPrimitivesResponse)) {
    throw new Error('Expected argument of type ListPrimitivesResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_ListPrimitivesResponse(buffer_arg) {
  return core_pb.ListPrimitivesResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_LoadFittedSolutionRequest(arg) {
  if (!(arg instanceof core_pb.LoadFittedSolutionRequest)) {
    throw new Error('Expected argument of type LoadFittedSolutionRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_LoadFittedSolutionRequest(buffer_arg) {
  return core_pb.LoadFittedSolutionRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_LoadFittedSolutionResponse(arg) {
  if (!(arg instanceof core_pb.LoadFittedSolutionResponse)) {
    throw new Error('Expected argument of type LoadFittedSolutionResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_LoadFittedSolutionResponse(buffer_arg) {
  return core_pb.LoadFittedSolutionResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_LoadSolutionRequest(arg) {
  if (!(arg instanceof core_pb.LoadSolutionRequest)) {
    throw new Error('Expected argument of type LoadSolutionRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_LoadSolutionRequest(buffer_arg) {
  return core_pb.LoadSolutionRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_LoadSolutionResponse(arg) {
  if (!(arg instanceof core_pb.LoadSolutionResponse)) {
    throw new Error('Expected argument of type LoadSolutionResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_LoadSolutionResponse(buffer_arg) {
  return core_pb.LoadSolutionResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ProduceSolutionRequest(arg) {
  if (!(arg instanceof core_pb.ProduceSolutionRequest)) {
    throw new Error('Expected argument of type ProduceSolutionRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_ProduceSolutionRequest(buffer_arg) {
  return core_pb.ProduceSolutionRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ProduceSolutionResponse(arg) {
  if (!(arg instanceof core_pb.ProduceSolutionResponse)) {
    throw new Error('Expected argument of type ProduceSolutionResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_ProduceSolutionResponse(buffer_arg) {
  return core_pb.ProduceSolutionResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_SaveFittedSolutionRequest(arg) {
  if (!(arg instanceof core_pb.SaveFittedSolutionRequest)) {
    throw new Error('Expected argument of type SaveFittedSolutionRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_SaveFittedSolutionRequest(buffer_arg) {
  return core_pb.SaveFittedSolutionRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_SaveFittedSolutionResponse(arg) {
  if (!(arg instanceof core_pb.SaveFittedSolutionResponse)) {
    throw new Error('Expected argument of type SaveFittedSolutionResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_SaveFittedSolutionResponse(buffer_arg) {
  return core_pb.SaveFittedSolutionResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_SaveSolutionRequest(arg) {
  if (!(arg instanceof core_pb.SaveSolutionRequest)) {
    throw new Error('Expected argument of type SaveSolutionRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_SaveSolutionRequest(buffer_arg) {
  return core_pb.SaveSolutionRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_SaveSolutionResponse(arg) {
  if (!(arg instanceof core_pb.SaveSolutionResponse)) {
    throw new Error('Expected argument of type SaveSolutionResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_SaveSolutionResponse(buffer_arg) {
  return core_pb.SaveSolutionResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ScorePredictionsRequest(arg) {
  if (!(arg instanceof core_pb.ScorePredictionsRequest)) {
    throw new Error('Expected argument of type ScorePredictionsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_ScorePredictionsRequest(buffer_arg) {
  return core_pb.ScorePredictionsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ScorePredictionsResponse(arg) {
  if (!(arg instanceof core_pb.ScorePredictionsResponse)) {
    throw new Error('Expected argument of type ScorePredictionsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_ScorePredictionsResponse(buffer_arg) {
  return core_pb.ScorePredictionsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ScoreSolutionRequest(arg) {
  if (!(arg instanceof core_pb.ScoreSolutionRequest)) {
    throw new Error('Expected argument of type ScoreSolutionRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_ScoreSolutionRequest(buffer_arg) {
  return core_pb.ScoreSolutionRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ScoreSolutionResponse(arg) {
  if (!(arg instanceof core_pb.ScoreSolutionResponse)) {
    throw new Error('Expected argument of type ScoreSolutionResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_ScoreSolutionResponse(buffer_arg) {
  return core_pb.ScoreSolutionResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_SearchSolutionsRequest(arg) {
  if (!(arg instanceof core_pb.SearchSolutionsRequest)) {
    throw new Error('Expected argument of type SearchSolutionsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_SearchSolutionsRequest(buffer_arg) {
  return core_pb.SearchSolutionsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_SearchSolutionsResponse(arg) {
  if (!(arg instanceof core_pb.SearchSolutionsResponse)) {
    throw new Error('Expected argument of type SearchSolutionsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_SearchSolutionsResponse(buffer_arg) {
  return core_pb.SearchSolutionsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_SolutionExportRequest(arg) {
  if (!(arg instanceof core_pb.SolutionExportRequest)) {
    throw new Error('Expected argument of type SolutionExportRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_SolutionExportRequest(buffer_arg) {
  return core_pb.SolutionExportRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_SolutionExportResponse(arg) {
  if (!(arg instanceof core_pb.SolutionExportResponse)) {
    throw new Error('Expected argument of type SolutionExportResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_SolutionExportResponse(buffer_arg) {
  return core_pb.SolutionExportResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_SplitDataRequest(arg) {
  if (!(arg instanceof core_pb.SplitDataRequest)) {
    throw new Error('Expected argument of type SplitDataRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_SplitDataRequest(buffer_arg) {
  return core_pb.SplitDataRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_SplitDataResponse(arg) {
  if (!(arg instanceof core_pb.SplitDataResponse)) {
    throw new Error('Expected argument of type SplitDataResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_SplitDataResponse(buffer_arg) {
  return core_pb.SplitDataResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_StopSearchSolutionsRequest(arg) {
  if (!(arg instanceof core_pb.StopSearchSolutionsRequest)) {
    throw new Error('Expected argument of type StopSearchSolutionsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_StopSearchSolutionsRequest(buffer_arg) {
  return core_pb.StopSearchSolutionsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_StopSearchSolutionsResponse(arg) {
  if (!(arg instanceof core_pb.StopSearchSolutionsResponse)) {
    throw new Error('Expected argument of type StopSearchSolutionsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_StopSearchSolutionsResponse(buffer_arg) {
  return core_pb.StopSearchSolutionsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


// See each message's comments for information about each particular call.
var CoreService = exports.CoreService = {
  searchSolutions: {
    path: '/Core/SearchSolutions',
    requestStream: false,
    responseStream: false,
    requestType: core_pb.SearchSolutionsRequest,
    responseType: core_pb.SearchSolutionsResponse,
    requestSerialize: serialize_SearchSolutionsRequest,
    requestDeserialize: deserialize_SearchSolutionsRequest,
    responseSerialize: serialize_SearchSolutionsResponse,
    responseDeserialize: deserialize_SearchSolutionsResponse,
  },
  getSearchSolutionsResults: {
    path: '/Core/GetSearchSolutionsResults',
    requestStream: false,
    responseStream: true,
    requestType: core_pb.GetSearchSolutionsResultsRequest,
    responseType: core_pb.GetSearchSolutionsResultsResponse,
    requestSerialize: serialize_GetSearchSolutionsResultsRequest,
    requestDeserialize: deserialize_GetSearchSolutionsResultsRequest,
    responseSerialize: serialize_GetSearchSolutionsResultsResponse,
    responseDeserialize: deserialize_GetSearchSolutionsResultsResponse,
  },
  endSearchSolutions: {
    path: '/Core/EndSearchSolutions',
    requestStream: false,
    responseStream: false,
    requestType: core_pb.EndSearchSolutionsRequest,
    responseType: core_pb.EndSearchSolutionsResponse,
    requestSerialize: serialize_EndSearchSolutionsRequest,
    requestDeserialize: deserialize_EndSearchSolutionsRequest,
    responseSerialize: serialize_EndSearchSolutionsResponse,
    responseDeserialize: deserialize_EndSearchSolutionsResponse,
  },
  stopSearchSolutions: {
    path: '/Core/StopSearchSolutions',
    requestStream: false,
    responseStream: false,
    requestType: core_pb.StopSearchSolutionsRequest,
    responseType: core_pb.StopSearchSolutionsResponse,
    requestSerialize: serialize_StopSearchSolutionsRequest,
    requestDeserialize: deserialize_StopSearchSolutionsRequest,
    responseSerialize: serialize_StopSearchSolutionsResponse,
    responseDeserialize: deserialize_StopSearchSolutionsResponse,
  },
  describeSolution: {
    path: '/Core/DescribeSolution',
    requestStream: false,
    responseStream: false,
    requestType: core_pb.DescribeSolutionRequest,
    responseType: core_pb.DescribeSolutionResponse,
    requestSerialize: serialize_DescribeSolutionRequest,
    requestDeserialize: deserialize_DescribeSolutionRequest,
    responseSerialize: serialize_DescribeSolutionResponse,
    responseDeserialize: deserialize_DescribeSolutionResponse,
  },
  scoreSolution: {
    path: '/Core/ScoreSolution',
    requestStream: false,
    responseStream: false,
    requestType: core_pb.ScoreSolutionRequest,
    responseType: core_pb.ScoreSolutionResponse,
    requestSerialize: serialize_ScoreSolutionRequest,
    requestDeserialize: deserialize_ScoreSolutionRequest,
    responseSerialize: serialize_ScoreSolutionResponse,
    responseDeserialize: deserialize_ScoreSolutionResponse,
  },
  getScoreSolutionResults: {
    path: '/Core/GetScoreSolutionResults',
    requestStream: false,
    responseStream: true,
    requestType: core_pb.GetScoreSolutionResultsRequest,
    responseType: core_pb.GetScoreSolutionResultsResponse,
    requestSerialize: serialize_GetScoreSolutionResultsRequest,
    requestDeserialize: deserialize_GetScoreSolutionResultsRequest,
    responseSerialize: serialize_GetScoreSolutionResultsResponse,
    responseDeserialize: deserialize_GetScoreSolutionResultsResponse,
  },
  fitSolution: {
    path: '/Core/FitSolution',
    requestStream: false,
    responseStream: false,
    requestType: core_pb.FitSolutionRequest,
    responseType: core_pb.FitSolutionResponse,
    requestSerialize: serialize_FitSolutionRequest,
    requestDeserialize: deserialize_FitSolutionRequest,
    responseSerialize: serialize_FitSolutionResponse,
    responseDeserialize: deserialize_FitSolutionResponse,
  },
  getFitSolutionResults: {
    path: '/Core/GetFitSolutionResults',
    requestStream: false,
    responseStream: true,
    requestType: core_pb.GetFitSolutionResultsRequest,
    responseType: core_pb.GetFitSolutionResultsResponse,
    requestSerialize: serialize_GetFitSolutionResultsRequest,
    requestDeserialize: deserialize_GetFitSolutionResultsRequest,
    responseSerialize: serialize_GetFitSolutionResultsResponse,
    responseDeserialize: deserialize_GetFitSolutionResultsResponse,
  },
  produceSolution: {
    path: '/Core/ProduceSolution',
    requestStream: false,
    responseStream: false,
    requestType: core_pb.ProduceSolutionRequest,
    responseType: core_pb.ProduceSolutionResponse,
    requestSerialize: serialize_ProduceSolutionRequest,
    requestDeserialize: deserialize_ProduceSolutionRequest,
    responseSerialize: serialize_ProduceSolutionResponse,
    responseDeserialize: deserialize_ProduceSolutionResponse,
  },
  getProduceSolutionResults: {
    path: '/Core/GetProduceSolutionResults',
    requestStream: false,
    responseStream: true,
    requestType: core_pb.GetProduceSolutionResultsRequest,
    responseType: core_pb.GetProduceSolutionResultsResponse,
    requestSerialize: serialize_GetProduceSolutionResultsRequest,
    requestDeserialize: deserialize_GetProduceSolutionResultsRequest,
    responseSerialize: serialize_GetProduceSolutionResultsResponse,
    responseDeserialize: deserialize_GetProduceSolutionResultsResponse,
  },
  solutionExport: {
    path: '/Core/SolutionExport',
    requestStream: false,
    responseStream: false,
    requestType: core_pb.SolutionExportRequest,
    responseType: core_pb.SolutionExportResponse,
    requestSerialize: serialize_SolutionExportRequest,
    requestDeserialize: deserialize_SolutionExportRequest,
    responseSerialize: serialize_SolutionExportResponse,
    responseDeserialize: deserialize_SolutionExportResponse,
  },
  dataAvailable: {
    path: '/Core/DataAvailable',
    requestStream: false,
    responseStream: false,
    requestType: core_pb.DataAvailableRequest,
    responseType: core_pb.DataAvailableResponse,
    requestSerialize: serialize_DataAvailableRequest,
    requestDeserialize: deserialize_DataAvailableRequest,
    responseSerialize: serialize_DataAvailableResponse,
    responseDeserialize: deserialize_DataAvailableResponse,
  },
  listPrimitives: {
    path: '/Core/ListPrimitives',
    requestStream: false,
    responseStream: false,
    requestType: core_pb.ListPrimitivesRequest,
    responseType: core_pb.ListPrimitivesResponse,
    requestSerialize: serialize_ListPrimitivesRequest,
    requestDeserialize: deserialize_ListPrimitivesRequest,
    responseSerialize: serialize_ListPrimitivesResponse,
    responseDeserialize: deserialize_ListPrimitivesResponse,
  },
  hello: {
    path: '/Core/Hello',
    requestStream: false,
    responseStream: false,
    requestType: core_pb.HelloRequest,
    responseType: core_pb.HelloResponse,
    requestSerialize: serialize_HelloRequest,
    requestDeserialize: deserialize_HelloRequest,
    responseSerialize: serialize_HelloResponse,
    responseDeserialize: deserialize_HelloResponse,
  },
  // Optional.
saveSolution: {
    path: '/Core/SaveSolution',
    requestStream: false,
    responseStream: false,
    requestType: core_pb.SaveSolutionRequest,
    responseType: core_pb.SaveSolutionResponse,
    requestSerialize: serialize_SaveSolutionRequest,
    requestDeserialize: deserialize_SaveSolutionRequest,
    responseSerialize: serialize_SaveSolutionResponse,
    responseDeserialize: deserialize_SaveSolutionResponse,
  },
  loadSolution: {
    path: '/Core/LoadSolution',
    requestStream: false,
    responseStream: false,
    requestType: core_pb.LoadSolutionRequest,
    responseType: core_pb.LoadSolutionResponse,
    requestSerialize: serialize_LoadSolutionRequest,
    requestDeserialize: deserialize_LoadSolutionRequest,
    responseSerialize: serialize_LoadSolutionResponse,
    responseDeserialize: deserialize_LoadSolutionResponse,
  },
  saveFittedSolution: {
    path: '/Core/SaveFittedSolution',
    requestStream: false,
    responseStream: false,
    requestType: core_pb.SaveFittedSolutionRequest,
    responseType: core_pb.SaveFittedSolutionResponse,
    requestSerialize: serialize_SaveFittedSolutionRequest,
    requestDeserialize: deserialize_SaveFittedSolutionRequest,
    responseSerialize: serialize_SaveFittedSolutionResponse,
    responseDeserialize: deserialize_SaveFittedSolutionResponse,
  },
  loadFittedSolution: {
    path: '/Core/LoadFittedSolution',
    requestStream: false,
    responseStream: false,
    requestType: core_pb.LoadFittedSolutionRequest,
    responseType: core_pb.LoadFittedSolutionResponse,
    requestSerialize: serialize_LoadFittedSolutionRequest,
    requestDeserialize: deserialize_LoadFittedSolutionRequest,
    responseSerialize: serialize_LoadFittedSolutionResponse,
    responseDeserialize: deserialize_LoadFittedSolutionResponse,
  },
  splitData: {
    path: '/Core/SplitData',
    requestStream: false,
    responseStream: true,
    requestType: core_pb.SplitDataRequest,
    responseType: core_pb.SplitDataResponse,
    requestSerialize: serialize_SplitDataRequest,
    requestDeserialize: deserialize_SplitDataRequest,
    responseSerialize: serialize_SplitDataResponse,
    responseDeserialize: deserialize_SplitDataResponse,
  },
  scorePredictions: {
    path: '/Core/ScorePredictions',
    requestStream: false,
    responseStream: false,
    requestType: core_pb.ScorePredictionsRequest,
    responseType: core_pb.ScorePredictionsResponse,
    requestSerialize: serialize_ScorePredictionsRequest,
    requestDeserialize: deserialize_ScorePredictionsRequest,
    responseSerialize: serialize_ScorePredictionsResponse,
    responseDeserialize: deserialize_ScorePredictionsResponse,
  },
};

exports.CoreClient = grpc.makeGenericClientConstructor(CoreService);
