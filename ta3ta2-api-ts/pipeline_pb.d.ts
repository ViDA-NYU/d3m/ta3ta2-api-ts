// package: 
// file: pipeline.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as primitive_pb from "./primitive_pb";
import * as value_pb from "./value_pb";

export class ContainerArgument extends jspb.Message {
  getData(): string;
  setData(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ContainerArgument.AsObject;
  static toObject(includeInstance: boolean, msg: ContainerArgument): ContainerArgument.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ContainerArgument, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ContainerArgument;
  static deserializeBinaryFromReader(message: ContainerArgument, reader: jspb.BinaryReader): ContainerArgument;
}

export namespace ContainerArgument {
  export type AsObject = {
    data: string,
  }
}

export class ContainerArguments extends jspb.Message {
  clearDataList(): void;
  getDataList(): Array<string>;
  setDataList(value: Array<string>): void;
  addData(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ContainerArguments.AsObject;
  static toObject(includeInstance: boolean, msg: ContainerArguments): ContainerArguments.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ContainerArguments, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ContainerArguments;
  static deserializeBinaryFromReader(message: ContainerArguments, reader: jspb.BinaryReader): ContainerArguments;
}

export namespace ContainerArguments {
  export type AsObject = {
    dataList: Array<string>,
  }
}

export class DataArgument extends jspb.Message {
  getData(): string;
  setData(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DataArgument.AsObject;
  static toObject(includeInstance: boolean, msg: DataArgument): DataArgument.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DataArgument, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DataArgument;
  static deserializeBinaryFromReader(message: DataArgument, reader: jspb.BinaryReader): DataArgument;
}

export namespace DataArgument {
  export type AsObject = {
    data: string,
  }
}

export class DataArguments extends jspb.Message {
  clearDataList(): void;
  getDataList(): Array<string>;
  setDataList(value: Array<string>): void;
  addData(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DataArguments.AsObject;
  static toObject(includeInstance: boolean, msg: DataArguments): DataArguments.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DataArguments, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DataArguments;
  static deserializeBinaryFromReader(message: DataArguments, reader: jspb.BinaryReader): DataArguments;
}

export namespace DataArguments {
  export type AsObject = {
    dataList: Array<string>,
  }
}

export class PrimitiveArgument extends jspb.Message {
  getData(): number;
  setData(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PrimitiveArgument.AsObject;
  static toObject(includeInstance: boolean, msg: PrimitiveArgument): PrimitiveArgument.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PrimitiveArgument, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PrimitiveArgument;
  static deserializeBinaryFromReader(message: PrimitiveArgument, reader: jspb.BinaryReader): PrimitiveArgument;
}

export namespace PrimitiveArgument {
  export type AsObject = {
    data: number,
  }
}

export class PrimitiveArguments extends jspb.Message {
  clearDataList(): void;
  getDataList(): Array<number>;
  setDataList(value: Array<number>): void;
  addData(value: number, index?: number): number;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PrimitiveArguments.AsObject;
  static toObject(includeInstance: boolean, msg: PrimitiveArguments): PrimitiveArguments.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PrimitiveArguments, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PrimitiveArguments;
  static deserializeBinaryFromReader(message: PrimitiveArguments, reader: jspb.BinaryReader): PrimitiveArguments;
}

export namespace PrimitiveArguments {
  export type AsObject = {
    dataList: Array<number>,
  }
}

export class ValueArgument extends jspb.Message {
  hasData(): boolean;
  clearData(): void;
  getData(): value_pb.Value | undefined;
  setData(value?: value_pb.Value): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ValueArgument.AsObject;
  static toObject(includeInstance: boolean, msg: ValueArgument): ValueArgument.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ValueArgument, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ValueArgument;
  static deserializeBinaryFromReader(message: ValueArgument, reader: jspb.BinaryReader): ValueArgument;
}

export namespace ValueArgument {
  export type AsObject = {
    data?: value_pb.Value.AsObject,
  }
}

export class PrimitiveStepArgument extends jspb.Message {
  hasContainer(): boolean;
  clearContainer(): void;
  getContainer(): ContainerArgument | undefined;
  setContainer(value?: ContainerArgument): void;

  hasData(): boolean;
  clearData(): void;
  getData(): DataArgument | undefined;
  setData(value?: DataArgument): void;

  hasContainerList(): boolean;
  clearContainerList(): void;
  getContainerList(): ContainerArguments | undefined;
  setContainerList(value?: ContainerArguments): void;

  getArgumentCase(): PrimitiveStepArgument.ArgumentCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PrimitiveStepArgument.AsObject;
  static toObject(includeInstance: boolean, msg: PrimitiveStepArgument): PrimitiveStepArgument.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PrimitiveStepArgument, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PrimitiveStepArgument;
  static deserializeBinaryFromReader(message: PrimitiveStepArgument, reader: jspb.BinaryReader): PrimitiveStepArgument;
}

export namespace PrimitiveStepArgument {
  export type AsObject = {
    container?: ContainerArgument.AsObject,
    data?: DataArgument.AsObject,
    containerList?: ContainerArguments.AsObject,
  }

  export enum ArgumentCase {
    ARGUMENT_NOT_SET = 0,
    CONTAINER = 1,
    DATA = 2,
    CONTAINER_LIST = 3,
  }
}

export class PrimitiveStepHyperparameter extends jspb.Message {
  hasContainer(): boolean;
  clearContainer(): void;
  getContainer(): ContainerArgument | undefined;
  setContainer(value?: ContainerArgument): void;

  hasData(): boolean;
  clearData(): void;
  getData(): DataArgument | undefined;
  setData(value?: DataArgument): void;

  hasPrimitive(): boolean;
  clearPrimitive(): void;
  getPrimitive(): PrimitiveArgument | undefined;
  setPrimitive(value?: PrimitiveArgument): void;

  hasValue(): boolean;
  clearValue(): void;
  getValue(): ValueArgument | undefined;
  setValue(value?: ValueArgument): void;

  hasDataSet(): boolean;
  clearDataSet(): void;
  getDataSet(): DataArguments | undefined;
  setDataSet(value?: DataArguments): void;

  hasPrimitivesSet(): boolean;
  clearPrimitivesSet(): void;
  getPrimitivesSet(): PrimitiveArguments | undefined;
  setPrimitivesSet(value?: PrimitiveArguments): void;

  getArgumentCase(): PrimitiveStepHyperparameter.ArgumentCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PrimitiveStepHyperparameter.AsObject;
  static toObject(includeInstance: boolean, msg: PrimitiveStepHyperparameter): PrimitiveStepHyperparameter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PrimitiveStepHyperparameter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PrimitiveStepHyperparameter;
  static deserializeBinaryFromReader(message: PrimitiveStepHyperparameter, reader: jspb.BinaryReader): PrimitiveStepHyperparameter;
}

export namespace PrimitiveStepHyperparameter {
  export type AsObject = {
    container?: ContainerArgument.AsObject,
    data?: DataArgument.AsObject,
    primitive?: PrimitiveArgument.AsObject,
    value?: ValueArgument.AsObject,
    dataSet?: DataArguments.AsObject,
    primitivesSet?: PrimitiveArguments.AsObject,
  }

  export enum ArgumentCase {
    ARGUMENT_NOT_SET = 0,
    CONTAINER = 1,
    DATA = 2,
    PRIMITIVE = 3,
    VALUE = 4,
    DATA_SET = 5,
    PRIMITIVES_SET = 6,
  }
}

export class StepInput extends jspb.Message {
  getData(): string;
  setData(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StepInput.AsObject;
  static toObject(includeInstance: boolean, msg: StepInput): StepInput.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StepInput, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StepInput;
  static deserializeBinaryFromReader(message: StepInput, reader: jspb.BinaryReader): StepInput;
}

export namespace StepInput {
  export type AsObject = {
    data: string,
  }
}

export class StepOutput extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StepOutput.AsObject;
  static toObject(includeInstance: boolean, msg: StepOutput): StepOutput.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StepOutput, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StepOutput;
  static deserializeBinaryFromReader(message: StepOutput, reader: jspb.BinaryReader): StepOutput;
}

export namespace StepOutput {
  export type AsObject = {
    id: string,
  }
}

export class PipelineSource extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  getContact(): string;
  setContact(value: string): void;

  clearPipelinesList(): void;
  getPipelinesList(): Array<PipelineDescription>;
  setPipelinesList(value: Array<PipelineDescription>): void;
  addPipelines(value?: PipelineDescription, index?: number): PipelineDescription;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PipelineSource.AsObject;
  static toObject(includeInstance: boolean, msg: PipelineSource): PipelineSource.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PipelineSource, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PipelineSource;
  static deserializeBinaryFromReader(message: PipelineSource, reader: jspb.BinaryReader): PipelineSource;
}

export namespace PipelineSource {
  export type AsObject = {
    name: string,
    contact: string,
    pipelinesList: Array<PipelineDescription.AsObject>,
  }
}

export class PipelineDescriptionUser extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getReason(): string;
  setReason(value: string): void;

  getRationale(): string;
  setRationale(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PipelineDescriptionUser.AsObject;
  static toObject(includeInstance: boolean, msg: PipelineDescriptionUser): PipelineDescriptionUser.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PipelineDescriptionUser, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PipelineDescriptionUser;
  static deserializeBinaryFromReader(message: PipelineDescriptionUser, reader: jspb.BinaryReader): PipelineDescriptionUser;
}

export namespace PipelineDescriptionUser {
  export type AsObject = {
    id: string,
    reason: string,
    rationale: string,
  }
}

export class PipelineDescriptionInput extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PipelineDescriptionInput.AsObject;
  static toObject(includeInstance: boolean, msg: PipelineDescriptionInput): PipelineDescriptionInput.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PipelineDescriptionInput, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PipelineDescriptionInput;
  static deserializeBinaryFromReader(message: PipelineDescriptionInput, reader: jspb.BinaryReader): PipelineDescriptionInput;
}

export namespace PipelineDescriptionInput {
  export type AsObject = {
    name: string,
  }
}

export class PipelineDescriptionOutput extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  getData(): string;
  setData(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PipelineDescriptionOutput.AsObject;
  static toObject(includeInstance: boolean, msg: PipelineDescriptionOutput): PipelineDescriptionOutput.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PipelineDescriptionOutput, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PipelineDescriptionOutput;
  static deserializeBinaryFromReader(message: PipelineDescriptionOutput, reader: jspb.BinaryReader): PipelineDescriptionOutput;
}

export namespace PipelineDescriptionOutput {
  export type AsObject = {
    name: string,
    data: string,
  }
}

export class PrimitivePipelineDescriptionStep extends jspb.Message {
  hasPrimitive(): boolean;
  clearPrimitive(): void;
  getPrimitive(): primitive_pb.Primitive | undefined;
  setPrimitive(value?: primitive_pb.Primitive): void;

  getArgumentsMap(): jspb.Map<string, PrimitiveStepArgument>;
  clearArgumentsMap(): void;
  clearOutputsList(): void;
  getOutputsList(): Array<StepOutput>;
  setOutputsList(value: Array<StepOutput>): void;
  addOutputs(value?: StepOutput, index?: number): StepOutput;

  getHyperparamsMap(): jspb.Map<string, PrimitiveStepHyperparameter>;
  clearHyperparamsMap(): void;
  clearUsersList(): void;
  getUsersList(): Array<PipelineDescriptionUser>;
  setUsersList(value: Array<PipelineDescriptionUser>): void;
  addUsers(value?: PipelineDescriptionUser, index?: number): PipelineDescriptionUser;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PrimitivePipelineDescriptionStep.AsObject;
  static toObject(includeInstance: boolean, msg: PrimitivePipelineDescriptionStep): PrimitivePipelineDescriptionStep.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PrimitivePipelineDescriptionStep, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PrimitivePipelineDescriptionStep;
  static deserializeBinaryFromReader(message: PrimitivePipelineDescriptionStep, reader: jspb.BinaryReader): PrimitivePipelineDescriptionStep;
}

export namespace PrimitivePipelineDescriptionStep {
  export type AsObject = {
    primitive?: primitive_pb.Primitive.AsObject,
    argumentsMap: Array<[string, PrimitiveStepArgument.AsObject]>,
    outputsList: Array<StepOutput.AsObject>,
    hyperparamsMap: Array<[string, PrimitiveStepHyperparameter.AsObject]>,
    usersList: Array<PipelineDescriptionUser.AsObject>,
  }
}

export class SubpipelinePipelineDescriptionStep extends jspb.Message {
  hasPipeline(): boolean;
  clearPipeline(): void;
  getPipeline(): PipelineDescription | undefined;
  setPipeline(value?: PipelineDescription): void;

  clearInputsList(): void;
  getInputsList(): Array<StepInput>;
  setInputsList(value: Array<StepInput>): void;
  addInputs(value?: StepInput, index?: number): StepInput;

  clearOutputsList(): void;
  getOutputsList(): Array<StepOutput>;
  setOutputsList(value: Array<StepOutput>): void;
  addOutputs(value?: StepOutput, index?: number): StepOutput;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SubpipelinePipelineDescriptionStep.AsObject;
  static toObject(includeInstance: boolean, msg: SubpipelinePipelineDescriptionStep): SubpipelinePipelineDescriptionStep.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SubpipelinePipelineDescriptionStep, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SubpipelinePipelineDescriptionStep;
  static deserializeBinaryFromReader(message: SubpipelinePipelineDescriptionStep, reader: jspb.BinaryReader): SubpipelinePipelineDescriptionStep;
}

export namespace SubpipelinePipelineDescriptionStep {
  export type AsObject = {
    pipeline?: PipelineDescription.AsObject,
    inputsList: Array<StepInput.AsObject>,
    outputsList: Array<StepOutput.AsObject>,
  }
}

export class PlaceholderPipelineDescriptionStep extends jspb.Message {
  clearInputsList(): void;
  getInputsList(): Array<StepInput>;
  setInputsList(value: Array<StepInput>): void;
  addInputs(value?: StepInput, index?: number): StepInput;

  clearOutputsList(): void;
  getOutputsList(): Array<StepOutput>;
  setOutputsList(value: Array<StepOutput>): void;
  addOutputs(value?: StepOutput, index?: number): StepOutput;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PlaceholderPipelineDescriptionStep.AsObject;
  static toObject(includeInstance: boolean, msg: PlaceholderPipelineDescriptionStep): PlaceholderPipelineDescriptionStep.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PlaceholderPipelineDescriptionStep, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PlaceholderPipelineDescriptionStep;
  static deserializeBinaryFromReader(message: PlaceholderPipelineDescriptionStep, reader: jspb.BinaryReader): PlaceholderPipelineDescriptionStep;
}

export namespace PlaceholderPipelineDescriptionStep {
  export type AsObject = {
    inputsList: Array<StepInput.AsObject>,
    outputsList: Array<StepOutput.AsObject>,
  }
}

export class PipelineDescriptionStep extends jspb.Message {
  hasPrimitive(): boolean;
  clearPrimitive(): void;
  getPrimitive(): PrimitivePipelineDescriptionStep | undefined;
  setPrimitive(value?: PrimitivePipelineDescriptionStep): void;

  hasPipeline(): boolean;
  clearPipeline(): void;
  getPipeline(): SubpipelinePipelineDescriptionStep | undefined;
  setPipeline(value?: SubpipelinePipelineDescriptionStep): void;

  hasPlaceholder(): boolean;
  clearPlaceholder(): void;
  getPlaceholder(): PlaceholderPipelineDescriptionStep | undefined;
  setPlaceholder(value?: PlaceholderPipelineDescriptionStep): void;

  getStepCase(): PipelineDescriptionStep.StepCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PipelineDescriptionStep.AsObject;
  static toObject(includeInstance: boolean, msg: PipelineDescriptionStep): PipelineDescriptionStep.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PipelineDescriptionStep, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PipelineDescriptionStep;
  static deserializeBinaryFromReader(message: PipelineDescriptionStep, reader: jspb.BinaryReader): PipelineDescriptionStep;
}

export namespace PipelineDescriptionStep {
  export type AsObject = {
    primitive?: PrimitivePipelineDescriptionStep.AsObject,
    pipeline?: SubpipelinePipelineDescriptionStep.AsObject,
    placeholder?: PlaceholderPipelineDescriptionStep.AsObject,
  }

  export enum StepCase {
    STEP_NOT_SET = 0,
    PRIMITIVE = 1,
    PIPELINE = 2,
    PLACEHOLDER = 3,
  }
}

export class PipelineDescription extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  hasSource(): boolean;
  clearSource(): void;
  getSource(): PipelineSource | undefined;
  setSource(value?: PipelineSource): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreated(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getContext(): PipelineContextMap[keyof PipelineContextMap];
  setContext(value: PipelineContextMap[keyof PipelineContextMap]): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  clearUsersList(): void;
  getUsersList(): Array<PipelineDescriptionUser>;
  setUsersList(value: Array<PipelineDescriptionUser>): void;
  addUsers(value?: PipelineDescriptionUser, index?: number): PipelineDescriptionUser;

  clearInputsList(): void;
  getInputsList(): Array<PipelineDescriptionInput>;
  setInputsList(value: Array<PipelineDescriptionInput>): void;
  addInputs(value?: PipelineDescriptionInput, index?: number): PipelineDescriptionInput;

  clearOutputsList(): void;
  getOutputsList(): Array<PipelineDescriptionOutput>;
  setOutputsList(value: Array<PipelineDescriptionOutput>): void;
  addOutputs(value?: PipelineDescriptionOutput, index?: number): PipelineDescriptionOutput;

  clearStepsList(): void;
  getStepsList(): Array<PipelineDescriptionStep>;
  setStepsList(value: Array<PipelineDescriptionStep>): void;
  addSteps(value?: PipelineDescriptionStep, index?: number): PipelineDescriptionStep;

  getDigest(): string;
  setDigest(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PipelineDescription.AsObject;
  static toObject(includeInstance: boolean, msg: PipelineDescription): PipelineDescription.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PipelineDescription, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PipelineDescription;
  static deserializeBinaryFromReader(message: PipelineDescription, reader: jspb.BinaryReader): PipelineDescription;
}

export namespace PipelineDescription {
  export type AsObject = {
    id: string,
    source?: PipelineSource.AsObject,
    created?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    context: PipelineContextMap[keyof PipelineContextMap],
    name: string,
    description: string,
    usersList: Array<PipelineDescriptionUser.AsObject>,
    inputsList: Array<PipelineDescriptionInput.AsObject>,
    outputsList: Array<PipelineDescriptionOutput.AsObject>,
    stepsList: Array<PipelineDescriptionStep.AsObject>,
    digest: string,
  }
}

export interface PipelineContextMap {
  PIPELINE_CONTEXT_UNKNOWN: 0;
  PRETRAINING: 1;
  TESTING: 2;
  EVALUATION: 3;
  PRODUCTION: 4;
}

export const PipelineContext: PipelineContextMap;

