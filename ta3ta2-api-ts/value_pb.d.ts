// package: 
// file: value.proto

import * as jspb from "google-protobuf";

export class ValueError extends jspb.Message {
  getMessage(): string;
  setMessage(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ValueError.AsObject;
  static toObject(includeInstance: boolean, msg: ValueError): ValueError.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ValueError, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ValueError;
  static deserializeBinaryFromReader(message: ValueError, reader: jspb.BinaryReader): ValueError;
}

export namespace ValueError {
  export type AsObject = {
    message: string,
  }
}

export class ValueList extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ValueRaw>;
  setItemsList(value: Array<ValueRaw>): void;
  addItems(value?: ValueRaw, index?: number): ValueRaw;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ValueList.AsObject;
  static toObject(includeInstance: boolean, msg: ValueList): ValueList.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ValueList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ValueList;
  static deserializeBinaryFromReader(message: ValueList, reader: jspb.BinaryReader): ValueList;
}

export namespace ValueList {
  export type AsObject = {
    itemsList: Array<ValueRaw.AsObject>,
  }
}

export class ValueDict extends jspb.Message {
  getItemsMap(): jspb.Map<string, ValueRaw>;
  clearItemsMap(): void;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ValueDict.AsObject;
  static toObject(includeInstance: boolean, msg: ValueDict): ValueDict.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ValueDict, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ValueDict;
  static deserializeBinaryFromReader(message: ValueDict, reader: jspb.BinaryReader): ValueDict;
}

export namespace ValueDict {
  export type AsObject = {
    itemsMap: Array<[string, ValueRaw.AsObject]>,
  }
}

export class ValueRaw extends jspb.Message {
  hasNull(): boolean;
  clearNull(): void;
  getNull(): NullValueMap[keyof NullValueMap];
  setNull(value: NullValueMap[keyof NullValueMap]): void;

  hasDouble(): boolean;
  clearDouble(): void;
  getDouble(): number;
  setDouble(value: number): void;

  hasInt64(): boolean;
  clearInt64(): void;
  getInt64(): number;
  setInt64(value: number): void;

  hasBool(): boolean;
  clearBool(): void;
  getBool(): boolean;
  setBool(value: boolean): void;

  hasString(): boolean;
  clearString(): void;
  getString(): string;
  setString(value: string): void;

  hasBytes(): boolean;
  clearBytes(): void;
  getBytes(): Uint8Array | string;
  getBytes_asU8(): Uint8Array;
  getBytes_asB64(): string;
  setBytes(value: Uint8Array | string): void;

  hasList(): boolean;
  clearList(): void;
  getList(): ValueList | undefined;
  setList(value?: ValueList): void;

  hasDict(): boolean;
  clearDict(): void;
  getDict(): ValueDict | undefined;
  setDict(value?: ValueDict): void;

  getRawCase(): ValueRaw.RawCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ValueRaw.AsObject;
  static toObject(includeInstance: boolean, msg: ValueRaw): ValueRaw.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ValueRaw, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ValueRaw;
  static deserializeBinaryFromReader(message: ValueRaw, reader: jspb.BinaryReader): ValueRaw;
}

export namespace ValueRaw {
  export type AsObject = {
    pb_null: NullValueMap[keyof NullValueMap],
    pb_double: number,
    int64: number,
    bool: boolean,
    string: string,
    bytes: Uint8Array | string,
    list?: ValueList.AsObject,
    dict?: ValueDict.AsObject,
  }

  export enum RawCase {
    RAW_NOT_SET = 0,
    NULL = 1,
    DOUBLE = 2,
    INT64 = 3,
    BOOL = 4,
    STRING = 5,
    BYTES = 6,
    LIST = 7,
    DICT = 8,
  }
}

export class Value extends jspb.Message {
  hasError(): boolean;
  clearError(): void;
  getError(): ValueError | undefined;
  setError(value?: ValueError): void;

  hasRaw(): boolean;
  clearRaw(): void;
  getRaw(): ValueRaw | undefined;
  setRaw(value?: ValueRaw): void;

  hasDatasetUri(): boolean;
  clearDatasetUri(): void;
  getDatasetUri(): string;
  setDatasetUri(value: string): void;

  hasCsvUri(): boolean;
  clearCsvUri(): void;
  getCsvUri(): string;
  setCsvUri(value: string): void;

  hasPickleUri(): boolean;
  clearPickleUri(): void;
  getPickleUri(): string;
  setPickleUri(value: string): void;

  hasPickleBlob(): boolean;
  clearPickleBlob(): void;
  getPickleBlob(): Uint8Array | string;
  getPickleBlob_asU8(): Uint8Array;
  getPickleBlob_asB64(): string;
  setPickleBlob(value: Uint8Array | string): void;

  hasPlasmaId(): boolean;
  clearPlasmaId(): void;
  getPlasmaId(): Uint8Array | string;
  getPlasmaId_asU8(): Uint8Array;
  getPlasmaId_asB64(): string;
  setPlasmaId(value: Uint8Array | string): void;

  getValueCase(): Value.ValueCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Value.AsObject;
  static toObject(includeInstance: boolean, msg: Value): Value.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Value, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Value;
  static deserializeBinaryFromReader(message: Value, reader: jspb.BinaryReader): Value;
}

export namespace Value {
  export type AsObject = {
    error?: ValueError.AsObject,
    raw?: ValueRaw.AsObject,
    datasetUri: string,
    csvUri: string,
    pickleUri: string,
    pickleBlob: Uint8Array | string,
    plasmaId: Uint8Array | string,
  }

  export enum ValueCase {
    VALUE_NOT_SET = 0,
    ERROR = 1,
    RAW = 2,
    DATASET_URI = 3,
    CSV_URI = 4,
    PICKLE_URI = 5,
    PICKLE_BLOB = 6,
    PLASMA_ID = 7,
  }
}

export interface ValueTypeMap {
  VALUE_TYPE_UNDEFINED: 0;
  RAW: 1;
  DATASET_URI: 2;
  CSV_URI: 3;
  PICKLE_URI: 4;
  PICKLE_BLOB: 5;
  PLASMA_ID: 6;
  LARGE_RAW: 7;
  LARGE_PICKLE_BLOB: 8;
}

export const ValueType: ValueTypeMap;

export interface NullValueMap {
  NULL_VALUE: 0;
}

export const NullValue: NullValueMap;

