// package: 
// file: primitive.proto

import * as jspb from "google-protobuf";

export class Primitive extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getVersion(): string;
  setVersion(value: string): void;

  getPythonPath(): string;
  setPythonPath(value: string): void;

  getName(): string;
  setName(value: string): void;

  getDigest(): string;
  setDigest(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Primitive.AsObject;
  static toObject(includeInstance: boolean, msg: Primitive): Primitive.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Primitive, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Primitive;
  static deserializeBinaryFromReader(message: Primitive, reader: jspb.BinaryReader): Primitive;
}

export namespace Primitive {
  export type AsObject = {
    id: string,
    version: string,
    pythonPath: string,
    name: string,
    digest: string,
  }
}

