# D3M TA3TA2-API Protocol for TypeScript

This project provides Javascript and Typescript compiled files for the TA3TA2-API gRPC protocol. The TA3TA2-API is a protocol for communication between AutoML systems (TA2) and user-facing systems (TA3) that allows the building and curation of machine learning models.

For more information about the D3M TA3TA2-API, visit: https://gitlab.com/datadrivendiscovery/ta3ta2-api/

For more details about this package, visit: https://gitlab.com/ViDA-NYU/d3m/ta3ta2-api-ts/

## Usage

```js
import { grpc } from 'ta3ta2-api-ts/grpc-lib';
import { CoreClient } from 'ta3ta2-api-ts/core_grpc_pb';

const grpcClient = new CoreClient(GRPC_CONN_URL, grpc.credentials.createInsecure());
```