// GENERATED CODE -- DO NOT EDIT!

// package: 
// file: core.proto

import * as core_pb from "./core_pb";
import * as grpc from "grpc";

interface ICoreService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
  searchSolutions: grpc.MethodDefinition<core_pb.SearchSolutionsRequest, core_pb.SearchSolutionsResponse>;
  getSearchSolutionsResults: grpc.MethodDefinition<core_pb.GetSearchSolutionsResultsRequest, core_pb.GetSearchSolutionsResultsResponse>;
  endSearchSolutions: grpc.MethodDefinition<core_pb.EndSearchSolutionsRequest, core_pb.EndSearchSolutionsResponse>;
  stopSearchSolutions: grpc.MethodDefinition<core_pb.StopSearchSolutionsRequest, core_pb.StopSearchSolutionsResponse>;
  describeSolution: grpc.MethodDefinition<core_pb.DescribeSolutionRequest, core_pb.DescribeSolutionResponse>;
  scoreSolution: grpc.MethodDefinition<core_pb.ScoreSolutionRequest, core_pb.ScoreSolutionResponse>;
  getScoreSolutionResults: grpc.MethodDefinition<core_pb.GetScoreSolutionResultsRequest, core_pb.GetScoreSolutionResultsResponse>;
  fitSolution: grpc.MethodDefinition<core_pb.FitSolutionRequest, core_pb.FitSolutionResponse>;
  getFitSolutionResults: grpc.MethodDefinition<core_pb.GetFitSolutionResultsRequest, core_pb.GetFitSolutionResultsResponse>;
  produceSolution: grpc.MethodDefinition<core_pb.ProduceSolutionRequest, core_pb.ProduceSolutionResponse>;
  getProduceSolutionResults: grpc.MethodDefinition<core_pb.GetProduceSolutionResultsRequest, core_pb.GetProduceSolutionResultsResponse>;
  solutionExport: grpc.MethodDefinition<core_pb.SolutionExportRequest, core_pb.SolutionExportResponse>;
  dataAvailable: grpc.MethodDefinition<core_pb.DataAvailableRequest, core_pb.DataAvailableResponse>;
  listPrimitives: grpc.MethodDefinition<core_pb.ListPrimitivesRequest, core_pb.ListPrimitivesResponse>;
  hello: grpc.MethodDefinition<core_pb.HelloRequest, core_pb.HelloResponse>;
  saveSolution: grpc.MethodDefinition<core_pb.SaveSolutionRequest, core_pb.SaveSolutionResponse>;
  loadSolution: grpc.MethodDefinition<core_pb.LoadSolutionRequest, core_pb.LoadSolutionResponse>;
  saveFittedSolution: grpc.MethodDefinition<core_pb.SaveFittedSolutionRequest, core_pb.SaveFittedSolutionResponse>;
  loadFittedSolution: grpc.MethodDefinition<core_pb.LoadFittedSolutionRequest, core_pb.LoadFittedSolutionResponse>;
  splitData: grpc.MethodDefinition<core_pb.SplitDataRequest, core_pb.SplitDataResponse>;
  scorePredictions: grpc.MethodDefinition<core_pb.ScorePredictionsRequest, core_pb.ScorePredictionsResponse>;
}

export const CoreService: ICoreService;

export class CoreClient extends grpc.Client {
  constructor(address: string, credentials: grpc.ChannelCredentials, options?: object);
  searchSolutions(argument: core_pb.SearchSolutionsRequest, callback: grpc.requestCallback<core_pb.SearchSolutionsResponse>): grpc.ClientUnaryCall;
  searchSolutions(argument: core_pb.SearchSolutionsRequest, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.SearchSolutionsResponse>): grpc.ClientUnaryCall;
  searchSolutions(argument: core_pb.SearchSolutionsRequest, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.SearchSolutionsResponse>): grpc.ClientUnaryCall;
  getSearchSolutionsResults(argument: core_pb.GetSearchSolutionsResultsRequest, metadataOrOptions?: grpc.Metadata | grpc.CallOptions | null): grpc.ClientReadableStream<core_pb.GetSearchSolutionsResultsResponse>;
  getSearchSolutionsResults(argument: core_pb.GetSearchSolutionsResultsRequest, metadata?: grpc.Metadata | null, options?: grpc.CallOptions | null): grpc.ClientReadableStream<core_pb.GetSearchSolutionsResultsResponse>;
  endSearchSolutions(argument: core_pb.EndSearchSolutionsRequest, callback: grpc.requestCallback<core_pb.EndSearchSolutionsResponse>): grpc.ClientUnaryCall;
  endSearchSolutions(argument: core_pb.EndSearchSolutionsRequest, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.EndSearchSolutionsResponse>): grpc.ClientUnaryCall;
  endSearchSolutions(argument: core_pb.EndSearchSolutionsRequest, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.EndSearchSolutionsResponse>): grpc.ClientUnaryCall;
  stopSearchSolutions(argument: core_pb.StopSearchSolutionsRequest, callback: grpc.requestCallback<core_pb.StopSearchSolutionsResponse>): grpc.ClientUnaryCall;
  stopSearchSolutions(argument: core_pb.StopSearchSolutionsRequest, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.StopSearchSolutionsResponse>): grpc.ClientUnaryCall;
  stopSearchSolutions(argument: core_pb.StopSearchSolutionsRequest, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.StopSearchSolutionsResponse>): grpc.ClientUnaryCall;
  describeSolution(argument: core_pb.DescribeSolutionRequest, callback: grpc.requestCallback<core_pb.DescribeSolutionResponse>): grpc.ClientUnaryCall;
  describeSolution(argument: core_pb.DescribeSolutionRequest, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.DescribeSolutionResponse>): grpc.ClientUnaryCall;
  describeSolution(argument: core_pb.DescribeSolutionRequest, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.DescribeSolutionResponse>): grpc.ClientUnaryCall;
  scoreSolution(argument: core_pb.ScoreSolutionRequest, callback: grpc.requestCallback<core_pb.ScoreSolutionResponse>): grpc.ClientUnaryCall;
  scoreSolution(argument: core_pb.ScoreSolutionRequest, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.ScoreSolutionResponse>): grpc.ClientUnaryCall;
  scoreSolution(argument: core_pb.ScoreSolutionRequest, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.ScoreSolutionResponse>): grpc.ClientUnaryCall;
  getScoreSolutionResults(argument: core_pb.GetScoreSolutionResultsRequest, metadataOrOptions?: grpc.Metadata | grpc.CallOptions | null): grpc.ClientReadableStream<core_pb.GetScoreSolutionResultsResponse>;
  getScoreSolutionResults(argument: core_pb.GetScoreSolutionResultsRequest, metadata?: grpc.Metadata | null, options?: grpc.CallOptions | null): grpc.ClientReadableStream<core_pb.GetScoreSolutionResultsResponse>;
  fitSolution(argument: core_pb.FitSolutionRequest, callback: grpc.requestCallback<core_pb.FitSolutionResponse>): grpc.ClientUnaryCall;
  fitSolution(argument: core_pb.FitSolutionRequest, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.FitSolutionResponse>): grpc.ClientUnaryCall;
  fitSolution(argument: core_pb.FitSolutionRequest, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.FitSolutionResponse>): grpc.ClientUnaryCall;
  getFitSolutionResults(argument: core_pb.GetFitSolutionResultsRequest, metadataOrOptions?: grpc.Metadata | grpc.CallOptions | null): grpc.ClientReadableStream<core_pb.GetFitSolutionResultsResponse>;
  getFitSolutionResults(argument: core_pb.GetFitSolutionResultsRequest, metadata?: grpc.Metadata | null, options?: grpc.CallOptions | null): grpc.ClientReadableStream<core_pb.GetFitSolutionResultsResponse>;
  produceSolution(argument: core_pb.ProduceSolutionRequest, callback: grpc.requestCallback<core_pb.ProduceSolutionResponse>): grpc.ClientUnaryCall;
  produceSolution(argument: core_pb.ProduceSolutionRequest, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.ProduceSolutionResponse>): grpc.ClientUnaryCall;
  produceSolution(argument: core_pb.ProduceSolutionRequest, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.ProduceSolutionResponse>): grpc.ClientUnaryCall;
  getProduceSolutionResults(argument: core_pb.GetProduceSolutionResultsRequest, metadataOrOptions?: grpc.Metadata | grpc.CallOptions | null): grpc.ClientReadableStream<core_pb.GetProduceSolutionResultsResponse>;
  getProduceSolutionResults(argument: core_pb.GetProduceSolutionResultsRequest, metadata?: grpc.Metadata | null, options?: grpc.CallOptions | null): grpc.ClientReadableStream<core_pb.GetProduceSolutionResultsResponse>;
  solutionExport(argument: core_pb.SolutionExportRequest, callback: grpc.requestCallback<core_pb.SolutionExportResponse>): grpc.ClientUnaryCall;
  solutionExport(argument: core_pb.SolutionExportRequest, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.SolutionExportResponse>): grpc.ClientUnaryCall;
  solutionExport(argument: core_pb.SolutionExportRequest, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.SolutionExportResponse>): grpc.ClientUnaryCall;
  dataAvailable(argument: core_pb.DataAvailableRequest, callback: grpc.requestCallback<core_pb.DataAvailableResponse>): grpc.ClientUnaryCall;
  dataAvailable(argument: core_pb.DataAvailableRequest, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.DataAvailableResponse>): grpc.ClientUnaryCall;
  dataAvailable(argument: core_pb.DataAvailableRequest, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.DataAvailableResponse>): grpc.ClientUnaryCall;
  listPrimitives(argument: core_pb.ListPrimitivesRequest, callback: grpc.requestCallback<core_pb.ListPrimitivesResponse>): grpc.ClientUnaryCall;
  listPrimitives(argument: core_pb.ListPrimitivesRequest, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.ListPrimitivesResponse>): grpc.ClientUnaryCall;
  listPrimitives(argument: core_pb.ListPrimitivesRequest, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.ListPrimitivesResponse>): grpc.ClientUnaryCall;
  hello(argument: core_pb.HelloRequest, callback: grpc.requestCallback<core_pb.HelloResponse>): grpc.ClientUnaryCall;
  hello(argument: core_pb.HelloRequest, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.HelloResponse>): grpc.ClientUnaryCall;
  hello(argument: core_pb.HelloRequest, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.HelloResponse>): grpc.ClientUnaryCall;
  saveSolution(argument: core_pb.SaveSolutionRequest, callback: grpc.requestCallback<core_pb.SaveSolutionResponse>): grpc.ClientUnaryCall;
  saveSolution(argument: core_pb.SaveSolutionRequest, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.SaveSolutionResponse>): grpc.ClientUnaryCall;
  saveSolution(argument: core_pb.SaveSolutionRequest, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.SaveSolutionResponse>): grpc.ClientUnaryCall;
  loadSolution(argument: core_pb.LoadSolutionRequest, callback: grpc.requestCallback<core_pb.LoadSolutionResponse>): grpc.ClientUnaryCall;
  loadSolution(argument: core_pb.LoadSolutionRequest, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.LoadSolutionResponse>): grpc.ClientUnaryCall;
  loadSolution(argument: core_pb.LoadSolutionRequest, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.LoadSolutionResponse>): grpc.ClientUnaryCall;
  saveFittedSolution(argument: core_pb.SaveFittedSolutionRequest, callback: grpc.requestCallback<core_pb.SaveFittedSolutionResponse>): grpc.ClientUnaryCall;
  saveFittedSolution(argument: core_pb.SaveFittedSolutionRequest, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.SaveFittedSolutionResponse>): grpc.ClientUnaryCall;
  saveFittedSolution(argument: core_pb.SaveFittedSolutionRequest, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.SaveFittedSolutionResponse>): grpc.ClientUnaryCall;
  loadFittedSolution(argument: core_pb.LoadFittedSolutionRequest, callback: grpc.requestCallback<core_pb.LoadFittedSolutionResponse>): grpc.ClientUnaryCall;
  loadFittedSolution(argument: core_pb.LoadFittedSolutionRequest, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.LoadFittedSolutionResponse>): grpc.ClientUnaryCall;
  loadFittedSolution(argument: core_pb.LoadFittedSolutionRequest, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.LoadFittedSolutionResponse>): grpc.ClientUnaryCall;
  splitData(argument: core_pb.SplitDataRequest, metadataOrOptions?: grpc.Metadata | grpc.CallOptions | null): grpc.ClientReadableStream<core_pb.SplitDataResponse>;
  splitData(argument: core_pb.SplitDataRequest, metadata?: grpc.Metadata | null, options?: grpc.CallOptions | null): grpc.ClientReadableStream<core_pb.SplitDataResponse>;
  scorePredictions(argument: core_pb.ScorePredictionsRequest, callback: grpc.requestCallback<core_pb.ScorePredictionsResponse>): grpc.ClientUnaryCall;
  scorePredictions(argument: core_pb.ScorePredictionsRequest, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.ScorePredictionsResponse>): grpc.ClientUnaryCall;
  scorePredictions(argument: core_pb.ScorePredictionsRequest, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<core_pb.ScorePredictionsResponse>): grpc.ClientUnaryCall;
}
