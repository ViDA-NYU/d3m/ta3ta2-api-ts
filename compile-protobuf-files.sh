# Path to the source .proto files
TA3TA2_SRC="./ta3ta2-api-src"

# Directory to write generated code to (.js and .d.ts files)
OUT_DIR="./ta3ta2-api-ts"

# Path to the protoc plugin
PROTOC_GEN_TS_PATH="${OUT_DIR}/node_modules/.bin/protoc-gen-ts"

# Path to the grpc_node_plugin
PROTOC_GEN_GRPC_PATH="${OUT_DIR}/node_modules/.bin/grpc_tools_node_protoc_plugin"

# Make sure that plugins are installed in the package folder
echo "Installing required protoc plugins..."
npm install --prefix $OUT_DIR

# Generate files
echo "Compiling .proto files to Typescript and Javascript..."
protoc \
    --plugin="protoc-gen-ts=${PROTOC_GEN_TS_PATH}" \
    --plugin=protoc-gen-grpc=${PROTOC_GEN_GRPC_PATH} \
    -I ${TA3TA2_SRC}/ \
    -I ${TA3TA2_SRC}/google/protobuf/ \
    --js_out="import_style=commonjs,binary:${OUT_DIR}" \
    --ts_out="service=grpc-node:${OUT_DIR}" \
    --grpc_out="${OUT_DIR}" \
    ${TA3TA2_SRC}/core.proto \
    ${TA3TA2_SRC}/pipeline.proto \
    ${TA3TA2_SRC}/primitive.proto \
    ${TA3TA2_SRC}/problem.proto \
    ${TA3TA2_SRC}/value.proto
echo "Done."