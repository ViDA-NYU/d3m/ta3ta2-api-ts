#!/bin/bash

fetch_script_dir=`dirname $0`
API_DIR=$fetch_script_dir/ta3ta2-api-src

env="osx"
uname_out=`uname -s`
case $uname_out in
  Linux*) env="linux";;
esac

[ -e $API_DIR ] && rm -r $API_DIR

if [ ! -e $API_DIR ]; then
  git clone https://gitlab.com/datadrivendiscovery/ta3ta2-api $API_DIR
  # copy the google protobuf protos to local locations
  curl -OL https://github.com/google/protobuf/releases/download/v3.3.0/protoc-3.3.0-$env-x86_64.zip
  unzip protoc-3.3.0-$env-x86_64.zip -d protoc3
  mv protoc3/include/google $API_DIR/
  rm protoc-3.3.0*.zip
  rm -r protoc3

  cd $API_DIR
  git checkout v2020.2.11
  cd -
  rm -rf $API_DIR/.git
fi
