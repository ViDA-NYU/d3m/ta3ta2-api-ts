# D3M TA3TA2-API Protocol for TypeScript

This project provides Javascript and Typescript compiled files for the TA3TA2-API gRPC protocol.

## Prerequisites

#### Install Protocol Buffers compiler (protoc)

##### Install required packages for building protoc
```
sudo apt-get install autoconf automake libtool curl make g++ unzip
```

##### Install protoc compiler

1. Download the latest version of `protobuf-all-[VERSION].tar.gz` from https://github.com/protocolbuffers/protobuf/releases

2. Extract the contents and change in the directory

3. Run:

```
./configure
make
make check
sudo make install
sudo ldconfig # refresh shared library cache.
```

4. Check if it works

```
$ protoc --version
libprotoc 3.11.2
```

## Upgrading the package to new TA3TA2-API protocol versions

To update the compiled files after a new release run:

1. Clone this repository:
```
git clone git@gitlab.com:ViDA-NYU/d3m/ta3ta2-api-ts.git
```
2. Update the `ta3ta2-api` repository tag in the `fetch-ta3ta2api.sh` script
3. Run the fetch script the get the desired version:
```
./fetch-ta3ta2api.sh
```
4. Compile the protocol files:
```
./compile-protobuf-files.sh
```